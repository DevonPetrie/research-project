# COS 700 Research Project

## Devon Petrie
## u17019266

---

## Project Description

Radio Astronomy Anomaly Detection using Deep Generative Models

Anomaly - A low probability event in the observed data.
  
---

# Research papers

### 1) ADAE - Anomaly Detection with Adversarial Dual Autoencoders
<a href="http://arxiv.org/abs/1902.06924"  target="_blank">Link to paper</a>

<a href="https://arxiv.org/pdf/1902.06924.pdf"  target="_blank">Link to pdf</a>

<ul>
  <li>
    GAN-based anomaly detection
  </li>
  <li>
    Adversarial Dual Autoencoders (ADAE) - consists of two autoencoders and generator and discriminator
  </li>
  <li>
    Discriminator reconstruction error as anomaly score
  </li>
  <li>
    Semi-supervised
  </li>
  <li>
    Discriminator pixel-wise reconstruction error
  </li>
  <li>
    encoder( G<sub>e</sub> )-decoder( G<sub>D</sub> )-encoder( E ) pipeline architecture, whereby G<sub>e</sub> learns to map X -> Z.
  </li>
  <li>
    Adversarial trainig objective (pixel-wise error between the reconstructed image of data x through G and of generated image G(x) through D) = ||G(x) - D(G(x))||<sub>1</sub>
  </li>
  <li>
    Discriminator training objective = L<sub>D</sub> = ||x - D(x)||<sub>1</sub> - ||G(x) - D(G(x))||<sub>1</sub>
  </li>
  <li>
    Generator training objective = L<sub>G</sub> = ||x - D(x)||<sub>1</sub> - ||G(x) - D(G(x))||<sub>1</sub>
  </li>
  <li>
    Anomaly score = A(x) = ||x - D(G(x))||<sub>2</sub>
  </li>
  <li>
    Faired better than GANomaly in brain MRI problem domain and on CIFAR-10 dataset
  </li>
</ul>

---

### 2) ADGAN - Anomaly detection with generative adversarial networks
<a href="https://openreview.net/pdf?id=S1EfylZ0Z"  target="_blank">Link to paper</a>

<ul>
  <li>
    GAN-based anomaly detection, after GAN training has converged.
  </li>
  <li>
    Assumes that, for a sample drawn from the data distribution, there exists some point in the GAN's latent space which, after passing it through the generator network, should closely resemble this sample.
  </li>
  <li>
    GAN's attempt to learn the parametrization of a neural network, the so-called generator <i>g<sub>0</sub></i>, that maps low-dimensional samples drawn from some simple noise prior <i>p<sub>z</sub></i> (e.g. a multivariate Gaussian) to samples in the image space, thereby inducing a distribution <i>q<sub>0</sub></i> (the push-forward of <i>p<sub>z</sub></i> with respect to <i>g<sub>0</sub></i>) that approximates <i>p</i>.
  </li>
  <li>
    Given a new sample <i>x ~ p</i>, there should exist a point <i>z</i> in the latent space, such that <i>g<sub>0</sub>(z) =(approx) x</i>. Additionally we expect points away from the support of <i>p</i> to have no representation in the latent space, or at least occupy a small portion of the probability mass in the latent distribution, since they are easily discerned by <i>d<sub>w</sub></i> as not coming from <i>p</i>.
  </il>
  <li>
    To find <i>z</i>, we initialize from <i>z<sub>0</sub> ~ p<sub>z</sub></i>, where <i>p<sub>z</sub></i> is the same noise prior also used during GAN training. For l = 1, ..., k steps, we backpropagate the reconstruction loss L between <i>g<sub>0</sub>(z<sub>l</sub>)</i> and <i>x</i>, making the subsequent generation <i>g<sub>0</sub>(z<sub>l+1</sub>)</i> more like <i>x</i>.
  </li>
  <li>
    The key idea for ADGAN is that if the generator was trained on the same distribution <i>x</i> was drawn from, then the average over the final set of reconstruction lisses will assume low values, and high values otherwise.
  </li>
</ul>

---

### 3) AEDI - Adversarial autoencoders for anomalous event detection in images
<a href="https://scholarworks.iupui.edu/handle/1805/12352"  target="_blank">Link to paper</a>

<a href="https://scholarworks.iupui.edu/bitstream/handle/1805/12352/adversarial-autoencoders-anomalous%20%2815%29.pdf?sequence=1&isAllowed=y"  target="_blank">Link to pdf</a>

<ul>
  <li>
    Anomaly - A low probability event in the observed data.
  </li>
  <li>
    Unsupervised, since abnormal classes are rare and in most cases nonexistent.
  </li>
  <li>
    Combination of Convolutional Autoencoders and Generative Adversarial Networks, called Adversarial Autoencoders.
  </li>
  <li>
    Architecture of the autoencoder
      <ul>
        <li>
          4 convolutional layers, each of which is followed by a dropout and a max pooling layer
          <ul>
            <li>
              Layers are connected with a leaky ReLU
            </li>
          </ul>
        </li>
        <li>
          1st convolutional layer has 512 filters (produces 512 feature maps with a resolution of 10x10)
        </li>
        <li>
          pooling layers have kernels of size 2x2 pixels and perform max pooling
        </li>
        <li>
          Rest of the convolutional layers: 256 feature maps of 10x10 pixels, 128 feature maps of 10x10 pixels, 64 feature maps of 10x10 respectively.
        </li>
        <li>
          Dropout is set to 0.5
        </li>
        <li>
          L1-L2 regulatization
        </li>
        <li>
          Discriminator consists of 256 hidden dimensions.
        </li>
      </ul>
  </li>
</ul>

---

### 4) GANomaly - Semi-Supervised Anomaly Detection via Adversarial Training
<a href="https://arxiv.org/abs/1805.06725"  target="_blank">Link to paper</a>

<a href="https://arxiv.org/pdf/1805.06725.pdf"  target="_blank">Link to pdf</a>

<ul>
  <li>
    Given a dataset <i>D</i> containing a large number of normal samples <b><i>X</i></b> for training, and relatively few abnormal examples <b><i>X'</i></b> for the test, a model <i>f</i> is optimized overs its parameters <i>&theta;</i>. <i>f</i> learns the data distribution <i>p<sub><b>X</b></sub></i> of the normal samples during training while identifying abnormal samples as outliers during testing by outputting an anomaly score <i>A(x)</i>, where x is a given test example. A larger <i>A(x)</i> indicates possible abnormalities within the test image since <i>f</i> learns to minimize the output score during training. <i>A(x)</i> is general in that it can detect unseen anomalies as being non-conforming to <i>p<sub><b>X</b></sub></i>.
  </li>
  <li>
    Adversarial training framework
  </li>
  <li>
    First model <b><i>D</i></b> (training dataset of only normal images) to learn its manifold, then detect the abnormal samples in <b><i>D'</i></b> as outliers during the inference stage.
  </li>
  <li>
    For a given test image <i>x</i>, a high anomaly score of <i>A(x)</i> indicates possible anomalies within the image.
  </li>
  <li>
    Evaluation criteria, <i>A(x) > &straightphi;</i> indicates an anomaly, where <i>&straightphi;</i> is the threshold.
  </li>
  <li>
    GANomaly pipeline:
      <ul>
        <li>
          <b>First sub-network</b> is a bow tie autoencoder network behaving as the generator part of the model. The generator learns the input data representation and reconstructs the input image via the use of an encoder and a decoder network, respectively.
        </li>
        <li>
          Generator <i>G</i> first reads an input image <i>x</i> and forward-passes it to its encoder network <i>G<sub>E</sub></i>. With the use of convolutional layers followed by batch-norm and leaky ReLU activation, respectively, <i>G<sub>E</sub></i> downscales <i>x</i> by compressing it to a vector <i>z</i>.
        </li>
        <li>
          The decoder part <i>G<sub>D</sub></i> of the generator network <i>G</i> adopts the architecutre of a DCGAN (Deep Convolutional GAN) generator, using convolutional transpose layers, ReLU activation and batch-norm together with a tanh layer at the end. This approach upscales the vector <i>z</i> to reconstruct the image <i>x</i> as <i>x'</i>.
        </li>
        <li>
          <b>Second sub-network</b> is the encoder network <i>E</i> that compresses the image <i>x'</i> that is reconstructed by the network <i>G</i>.
        </li>
        <li>
          E downscales <i>x'</i> to find its feature representation <i>z' = E(x')</i>.
        </li>
        <li>
          The dimension of vector <i>z'</i> is the same as that of <i>z</i> for consistent comparison.
        </li>
        <li>
          <b>Third sub-network</b> is the discriminator network <i>D</i> whose objective is to classify the input <i>x</i> and the output <i>x'</i> as real or fake, respectively.
        </li>
        <li>
          Standard discriminator network introduced in DCGAN.
        </li>
      </ul>
  </li>
  <li>
    When an abnormal image is forward-passed into the network <i>G</i>, <i>G<sub>D</sub></i> is not able to reconstruct the abnormalities even through <i>G<sub>E</sub></i> managed to map the input <i>X</i> to the latent vector <i>z</i>
  </li>
  <li>
    An output <i>X'</i> that has missed abnormalities can lead to the encoder network <i>E</i> mapping <i>X'</i> to a vector <i>z'</i> that has also missed abnormal feature representation, causing dissimilarity between <i>z</i> and <i>z'</i>. When there is such dissimilarity within latent vector space for an input image <i>X</i>, the model classifies <i>X</i> as an anomalous image.
  </li>
</ul>

---

### 5) GAN - Generative Adversarial Networks
<a href="https://arxiv.org/abs/1406.2661"  target="_blank">Link to paper</a>

<a href="https://arxiv.org/pdf/1406.2661.pdf"  target="_blank">Link to pdf</a>

---

### 6) FR classes - The morphology of extragalactic radio sources of high and low luminosity
<a href="https://academic.oup.com/mnras/article/167/1/31P/2604898"  target="_blank">Link to paper</a>

<a href="https://watermark.silverchair.com/mnras167-031P.pdf?token=AQECAHi208BE49Ooan9kkhW_Ercy7Dm3ZL_9Cf3qfKAc485ysgAAAn4wggJ6BgkqhkiG9w0BBwagggJrMIICZwIBADCCAmAGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMvxEQCFVKwb0n1o03AgEQgIICMUxPVD4SRHz9saGNR2Rt5R3KPLpWwKCNEy7Aw184ZcsaqfxokPjVh097VFiVsi7z5e99KwN9sk5yMHzqfVCdqU71sLLERY9PTP7bWaFA0C4gdmwc4-fFoTyJQ_RTxbYBRRE1pyWVd-74n6Eq6UntjiQYG0PGAdLor6tLjFFmPwHEV6cGuUL0ouUJGMiINQdPmsoffNSWHLvXqBrEVHv--fCWbE8B0jZPq9PhQC45yEXkj5_y2ArOlgDNZ4JADDHoJY48MHcXgWEWTkS5byc1C2ZnMnln02VqUkxDPmggzTaNKaV-zGdLuI1khQTEKNEhfbu_q-IR-rsD7zrOMetdVNYUSiS6G5ptmf5tlKZJyZUTG9p0k2FbcoY4dOwPLmTzvlj3zpqVHAwb2tOofnGAYJL2SK2Qrp2f4ZsvlXODLQksv9SI2Y7tMOJf4sg8PLKlZbQDbaSw2rjCStr0nD98BRwFi1kYsRPqeEPEV9Hj23CY7ZGtpzsJ07D88m7iuGC34wTbkRWJN6avnMn6bzXc_VZAqs9g68MLiNm4l0F-VtOnkKlC9CWb1gBh9X0Zd0olOzHHgJPkwRvwCWqAnszvqcKrI5yQIV7CvUFxfXVmhjijtQqhE1HWxjUtCi_9PdbmdxiTaoIDfiwObNdflExDowWCbCv0HLdmDMDnQ5SB9Jz84rXTqBmtBjkR-K_vQMMu6WU-KFrUhrkK0w4TLe0w0zx3HIeHGa9HB_sF_k4hROSicQ"  target="_blank">Link to pdf</a>

---

### 7) X shaped radio galaxy - FIRST “Winged” and X-Shaped Radio Source Candidates
<a href="https://iopscience.iop.org/article/10.1086/513095"  target="_blank">Link to paper</a>

<a href="https://iopscience.iop.org/article/10.1086/513095/pdf"  target="_blank">Link to pdf</a>

---

### 8) Variational Autoencoder - Auto-Encoding Variational Bayes
<a href="https://arxiv.org/abs/1312.6114"  target="_blank">Link to paper</a>

<a href="https://arxiv.org/pdf/1312.6114.pdf"  target="_blank">Link to pdf</a>

---

### 8) Disentangle autoencoders - B-VAE: Learning basic visual concepts with a constrained variational autoencoder
<a href="https://openreview.net/forum?id=Sy2fzU9gl"  target="_blank">Link to paper</a>

<a href="https://openreview.net/pdf?id=Sy2fzU9gl"  target="_blank">Link to pdf</a>