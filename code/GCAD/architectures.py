import tensorflow as tf

class mnist_architecture:
    def __init__(self):
        self.latent_dim = 8
        self.target_size = (28, 28)
        self.beta = 2.5
        self.labels = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
        self.anomaly_labels = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]

        self.batch_size = 128
        self.learning_rate = 0.0005
        self.optimiser = "adam"

    def get_encoder(self):
        encoder = tf.keras.Sequential(
        [
            tf.keras.layers.InputLayer(input_shape=(28, 28, 3)),
            tf.keras.layers.Conv2D(
                filters=64,
                kernel_size=5,
                strides=(2, 2),
                padding="same",
                activation="relu"
            ),
            tf.keras.layers.Conv2D(
                filters=32,
                kernel_size=5,
                strides=(2, 2),
                padding="same",
                activation="relu"
            ),
            tf.keras.layers.Flatten(),
            # No activation
            tf.keras.layers.Dense(self.latent_dim + self.latent_dim),
        ])

        return encoder

    def get_decoder(self):
        decoder = tf.keras.Sequential(
        [
            tf.keras.layers.InputLayer(input_shape=(self.latent_dim,)),
            tf.keras.layers.Dense(units=7*7*28, activation=tf.nn.relu),
            tf.keras.layers.Reshape(target_shape=(7, 7, 28)),
            tf.keras.layers.Conv2DTranspose(
                filters=32,
                kernel_size=5,
                strides=(2, 2),
                padding="same",
                activation="relu"
            ),
            tf.keras.layers.Conv2DTranspose(
                filters=64,
                kernel_size=5,
                strides=(2, 2),
                padding="same",
                activation="relu"
            ),
            tf.keras.layers.Conv2DTranspose(filters=3, kernel_size=5, strides=(1, 1), padding="same", activation="sigmoid"),
        ])

        return decoder

class cifar10_architecture:
    def __init__(self):
        self.latent_dim = 64
        self.target_size = (32, 32)
        self.beta = 15
        self.labels = ["airplane", "automobile", "bird", "cat", "deer", "dog", "frog", "horse", "ship", "truck"]
        self.anomaly_labels = ["airplane", "automobile", "bird", "cat", "deer", "dog", "frog", "horse", "ship", "truck"]

        self.batch_size = 128
        self.learning_rate = 0.0001
        self.optimiser = "rmsprop"

    def get_encoder(self):
        encoder = tf.keras.Sequential(
        [
            tf.keras.layers.InputLayer(input_shape=(32, 32, 3)),
            tf.keras.layers.Conv2D(
                filters=512,
                kernel_size=2,
                strides=(2, 2),
                padding="same",
                activation="relu"
            ),
            tf.keras.layers.Conv2D(
                filters=32,
                kernel_size=2,
                strides=(1, 1),
                padding="same",
                activation="relu"
            ),
            tf.keras.layers.Conv2D(
                filters=16,
                kernel_size=2,
                strides=(2, 2),
                padding="same",
                activation="relu"
            ),
            tf.keras.layers.Flatten(),
            # No activation
            tf.keras.layers.Dense(self.latent_dim + self.latent_dim),
        ])

        return encoder

    def get_decoder(self):
        decoder = tf.keras.Sequential(
        [
            tf.keras.layers.InputLayer(input_shape=(self.latent_dim,)),
            tf.keras.layers.Dense(units=8*8*32, activation=tf.nn.relu),
            tf.keras.layers.Reshape(target_shape=(8, 8, 32)),
            tf.keras.layers.Conv2DTranspose(
                filters=16,
                kernel_size=2,
                strides=(2, 2),
                padding="same",
                activation="relu"
            ),
            tf.keras.layers.Conv2DTranspose(
                filters=32,
                kernel_size=2,
                strides=(1, 1),
                padding="same",
                activation="relu"
            ),
            tf.keras.layers.Conv2DTranspose(
                filters=512,
                kernel_size=2,
                strides=(2, 2),
                padding="same",
                activation="relu"
            ),
            tf.keras.layers.Conv2DTranspose(filters=3, kernel_size=2, strides=(1, 1), padding="same", activation="sigmoid"),
        ])

        return decoder

class crack_architecture:
    def __init__(self):
        self.latent_dim = 64
        self.target_size = (128, 128)
        self.beta = 20
        self.labels = ["bridge_deck", "pavement", "wall"]
        self.anomaly_labels = ["bridge_deck", "pavement", "wall"]

        self.batch_size = 32
        self.learning_rate = 0.0005
        self.optimiser = "adam"

    def get_encoder(self):
        encoder = tf.keras.Sequential(
        [
            tf.keras.layers.InputLayer(input_shape=(128, 128, 3)),
            tf.keras.layers.Conv2D(
                filters=128,
                kernel_size=3,
                strides=(2, 2),
                padding="same",
                activation="relu"
            ),
            tf.keras.layers.Conv2D(
                filters=64,
                kernel_size=3,
                strides=(2, 2),
                padding="same",
                activation="relu"
            ),
            tf.keras.layers.Flatten(),
            # No activation
            tf.keras.layers.Dense(self.latent_dim + self.latent_dim),
        ])

        return encoder

    def get_decoder(self):
        decoder = tf.keras.Sequential(
        [
            tf.keras.layers.InputLayer(input_shape=(self.latent_dim,)),
            tf.keras.layers.Dense(units=16*16*128, activation=tf.nn.relu),
            tf.keras.layers.Reshape(target_shape=(16, 16, 128)),
            tf.keras.layers.Conv2DTranspose(
                filters=64,
                kernel_size=3,
                strides=(2, 2),
                padding="same",
                activation="relu"
            ),
            tf.keras.layers.Conv2DTranspose(
                filters=128,
                kernel_size=3,
                strides=(2, 2),
                padding="same",
                activation="relu"
            ),
            tf.keras.layers.Conv2DTranspose(filters=3, kernel_size=3, strides=(2, 2), padding="same", activation="sigmoid"),
        ])

        return decoder

class black_holes_architecture:
    def __init__(self):
        self.latent_dim = 1024
        self.target_size = (128, 128)
        self.beta = 5
        self.labels = ["positive", "negative"]
        self.anomaly_labels = ["negative"]

        self.batch_size = 64
        self.learning_rate = 0.0005
        self.optimiser = "adam"

    def get_encoder(self):
        encoder = tf.keras.Sequential(
        [
            tf.keras.layers.InputLayer(input_shape=(128, 128, 3)),
            tf.keras.layers.Conv2D(
                filters=128,
                kernel_size=3,
                strides=(2, 2),
                padding="same",
                activation="relu"
            ),
            tf.keras.layers.Conv2D(
                filters=64,
                kernel_size=3,
                strides=(2, 2),
                padding="same",
                activation="relu"
            ),
            tf.keras.layers.Flatten(),
            # No activation
            tf.keras.layers.Dense(self.latent_dim + self.latent_dim),
        ])

        return encoder

    def get_decoder(self):
        decoder = tf.keras.Sequential(
        [
            tf.keras.layers.InputLayer(input_shape=(self.latent_dim,)),
            tf.keras.layers.Dense(units=16*16*128, activation=tf.nn.relu),
            tf.keras.layers.Reshape(target_shape=(16, 16, 128)),
            tf.keras.layers.Conv2DTranspose(
                filters=64,
                kernel_size=3,
                strides=(2, 2),
                padding="same",
                activation="relu"
            ),
            tf.keras.layers.Conv2DTranspose(
                filters=128,
                kernel_size=3,
                strides=(2, 2),
                padding="same",
                activation="relu"
            ),
            tf.keras.layers.Conv2DTranspose(filters=3, kernel_size=3, strides=(2, 2), padding="same", activation="sigmoid"),
        ])

        return decoder