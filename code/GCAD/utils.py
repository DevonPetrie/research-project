from random import shuffle
from tabulate import tabulate
from sklearn.manifold import TSNE

import os
import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt

# Radar graph imports
# from matplotlib.patches import Circle, RegularPolygon
# from matplotlib.path import Path
# from matplotlib.projections.polar import PolarAxes
# from matplotlib.projections import register_projection
# from matplotlib.spines import Spine
# from matplotlib.transforms import Affine2D

# ----------------------------------------------
#             Main utility functions
# ----------------------------------------------
def distance_formula(point1, point2):
    x1, y1 = point1
    x2, y2 = point2

    return tf.math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)

def display_image(epoch_no, image_dir):
    return PIL.Image.open(image_dir + "image_at_epoch_{:04d}.png".format(epoch_no))

def removeOutliers(data):
    outlierConstant = 1.5

    upper_quartile = np.percentile(data, 75)
    lower_quartile = np.percentile(data, 25)

    IQR = (upper_quartile - lower_quartile) * outlierConstant

    quartileSet = (lower_quartile - IQR, upper_quartile + IQR)

    no_outliers = [data[i] for i in range(len(data)) if data[i] > quartileSet[0] and data[i] < quartileSet[1]]

    return no_outliers

def is_anomaly(model, image, image_no=0, verbose=0):
    is_anomaly = False

    # Pass the image through the network
    mean, logvar = model.encode(image)
    z = model.reparameterize(mean, logvar)

    prediction = model.sample(z)

    # Anomaly score
    # get square root of the pixel wise difference
    error = anomaly_score(image, prediction)

    # get the certainty of the prediction, i.e. how far it is away from the threshold
    certainty = (abs(error - model.error_threshold) / model.error_threshold) * 100

    if certainty > 100.0:
        certainty = 100

    if error > model.error_threshold: # anomaly
        is_anomaly = True

        if verbose > 0:
            title = "Anomaly, {:0.2f}% certainty".format(certainty)
            print("Anomaly!\nError = {:0.2f}\nCertainty = {:0.2f}".format(error, certainty))
    else:
        if verbose > 0:
            title = "Normal, {:0.2f}% certainty".format(certainty)
            print("Normal\nError = {:0.2f}\nCertainty = {:0.2f}".format(error, certainty))

    if verbose > 0:
        # settings for plotting
        nrows, ncols = 1, 2  # array of sub-plots

        # create figure (fig), and array of axes (ax)
        fig, axi = plt.subplots(nrows=nrows, ncols=ncols)

        fig.suptitle(title, fontsize=16)

        # plot simple raster image on each sub-plot
        axi[0].imshow(image[0], cmap="gray")
        axi[0].set_title("Original")

        axi[1].imshow(prediction[0], cmap="gray")
        axi[1].set_title("Reproduced, Anomaly score = {:0.4f}".format(error))

        plt.savefig(model.image_dir + "test_anomalies/image_{}.png".format(image_no))

        if verbose > 1:
            plt.show()
        else:
            plt.close()

    return is_anomaly

def get_anomaly_scores(model, dataset, num_samples):
    image_results = []

    if num_samples > dataset.samples:
        print("Number of samples requested ({}) is more than the number of samples given ({})".format(num_samples, dataset.samples))

        return image_results

    # Loading bar stuff
    length = num_samples
    i = 0
    loadBar = ">"
    nextBoundary = 2

    count = 0

    for image_batch, _ in dataset:

        with tf.device("/GPU:0"):
            predictions = model(image_batch)
        
        for k in range(len(predictions)):
            # image = image_batch[k].reshape(1, image_batch[k].shape[0], image_batch[k].shape[1], image_batch[k].shape[2]) # reshape to a batch of 1

            # print(anomaly_score(image, prediction))
            # print(model.discriminator(image))

            image_results.append(anomaly_score(image_batch[k], predictions[k]))# + model.discriminator(image)[0][0])
            # image_results.append(model.discriminator(image)[0][0])

            count += 1

            # Loading bar stuff
            # percentDone = int((count/length) * 100)

            # if percentDone >= nextBoundary:
            #     loadBar = "=" + loadBar
            #     nextBoundary += 2

            # print("\rDataset: {}% [{}{}] {}/{}".format(percentDone, loadBar, " " * (50-len(loadBar)), count, length), end="", flush=True)

            if count >= num_samples:
                # print("")
                return image_results    

def plot_score_distribution(model, normal_dataset, anomaly_dataset, num_samples=None):
    # Run through the normal and anomalous images to get their results
    if num_samples is not None:
        normal_images_results = get_anomaly_scores(model, normal_dataset, num_samples)
        anomalous_images_results = get_anomaly_scores(model, anomaly_dataset, num_samples)
    else:
        normal_images_results = get_anomaly_scores(model, normal_dataset, normal_dataset.samples)
        anomalous_images_results = get_anomaly_scores(model, anomaly_dataset, anomaly_dataset.samples)

    # Plot the results
    plt.plot(range(len(anomalous_images_results)), anomalous_images_results, "o", color="red", label="Anomalous images")
    plt.plot(range(len(normal_images_results)), normal_images_results, "o", color="blue", label="Normal images")

    plt.xticks([])

    plt.ylabel("Anomaly score (sqrt(MSE))")

    plt.legend()

    print("Saving figure to " + model.image_dir + "error_results.png")
    plt.savefig(model.image_dir + "error_results.png")

    plt.close()

    # Remove outliers
    normal_images_results_no_outliers = removeOutliers(normal_images_results)
    anomalous_images_results_no_outliers = removeOutliers(anomalous_images_results)

    # Plot the results (no outliers)
    plt.plot(range(len(normal_images_results_no_outliers)), normal_images_results_no_outliers, "o", color="blue", label="Normal images")
    plt.plot(range(len(anomalous_images_results_no_outliers)), anomalous_images_results_no_outliers, "o", color="red", label="Anomalous images")

    plt.xticks([])

    plt.ylabel("Anomaly score (sqrt(MSE))")

    plt.legend()

    print("Saving figure to " + model.image_dir + "error_results_no_outliers.png")
    plt.savefig(model.image_dir + "error_results_no_outliers.png")

    plt.close()

    # Normal images stats
    normal_avg = sum(normal_images_results) / len(normal_images_results)
    normal_max = max(normal_images_results)
    normal_min = min(normal_images_results)

    # Anomalous images stats
    anomaly_avg = sum(anomalous_images_results) / len(anomalous_images_results)
    anomaly_max = max(anomalous_images_results)
    anomaly_min = min(anomalous_images_results)

    # Normal images stats (no outliers)
    normal_no_outliers_avg = sum(normal_images_results_no_outliers) / len(normal_images_results_no_outliers)
    normal_no_outliers_max = max(normal_images_results_no_outliers)
    normal_no_outliers_min = min(normal_images_results_no_outliers)

    # Anomalous images stats (no outliers)
    anomaly_no_outliers_avg = sum(anomalous_images_results_no_outliers) / len(anomalous_images_results_no_outliers)
    anomaly_no_outliers_max = max(anomalous_images_results_no_outliers)
    anomaly_no_outliers_min = min(anomalous_images_results_no_outliers)

    print("------------------------------------")
    print("       Normal images results        ")
    print("------------------------------------")
    print("Normal images: Max = {}, Min = {}, Average = {}".format(normal_max, normal_min, normal_avg))
    print("Normal images (outliers removed): Max = {}, Min = {}, Average = {}\n".format(normal_no_outliers_max, normal_no_outliers_min, normal_no_outliers_avg))

    print("------------------------------------")
    print("      Anomalous images results      ")
    print("------------------------------------")
    print("Anomalous images: Max = {}, Min = {}, Average = {}".format(anomaly_max, anomaly_min, anomaly_avg))
    print("Anomalous images (outliers removed): Max = {}, Min = {}, Average = {}".format(anomaly_no_outliers_max, anomaly_no_outliers_min, anomaly_no_outliers_avg))

def confusion_matrix(model, normal_dataset, anomaly_dataset, num_samples=None, threshold=None):
    if threshold is None:
        threshold = model.error_threshold

    # Run through the normal and anomalous images to get their results
    if num_samples is not None:
        normal_images_results = get_anomaly_scores(model, normal_dataset, num_samples)
        anomalous_images_results = get_anomaly_scores(model, anomaly_dataset, num_samples)
    else:
        normal_images_results = get_anomaly_scores(model, normal_dataset, normal_dataset.samples)
        anomalous_images_results = get_anomaly_scores(model, anomaly_dataset, anomaly_dataset.samples)

    true_negatives = 0
    false_positives = 0

    true_positives = 0
    false_negatives = 0

    for error in normal_images_results:
        # If that image error was below the threshold (i.e. NOT an anomaly)
        if error < threshold:
            true_negatives += 1
        else:
            false_positives += 1

    for error in anomalous_images_results:
        # If that images error was above the threshold (i.e. An anomaly)
        if error > threshold:
            true_positives += 1
        else:
            false_negatives += 1

    # true positive rate (Sensitivity) = TP / (TP + FN)
    tpr = true_positives / (true_positives + false_negatives)

    # false positive rate = FP / (FP + TN)
    fpr = false_positives / (false_positives + true_negatives)

    # Specificity = TN / (TN + FP)
    # false positive rate = 1 - specificity
    specificity = true_negatives / (true_negatives + false_positives)

    # Accuracy = (TP + TN) / (TP + TN + FP + FN)
    accuracy = ((true_positives + true_negatives) / (true_positives + true_negatives + false_positives + false_negatives)) * 100

    # Negative predictive value (NPV) = TN / (TN + FN)
    npv = true_negatives / (true_negatives + false_negatives)

    # Precision = TP / (TP + FP)
    precision = true_positives / (true_positives + false_positives)

    #            |----------------|------------------|
    #            |    Positive    |     Negative     |
    # -----------|----------------|------------------|--------------
    #  Positive  |      TP        |        FN        |  Sensitivity
    # -----------|----------------|------------------|--------------
    #  Negative  |      FP        |        TN        |  Specificity
    # -----------|----------------|------------------|--------------
    #            |   Precision    |       NPV        |  Accuracy
    #            |----------------|------------------|--------------

    table_headers = ["", "Positive", "Negative", ""]

    row1 = [
        "Positive",
        "TP = {}".format(true_positives),
        "FN = {}".format(false_negatives),
        "Sensitivity = {:.4f}".format(tpr)
    ]

    row2 = [
        "Negative",
        "FP = {}".format(false_positives),
        "TN = {}".format(true_negatives),
        "Specificity = {:.4f}".format(specificity)
    ]

    row3 = [
        "",
        "Precision = {:.4f}".format(precision),
        "NPV = {:.4f}".format(npv),
        "Accuracy = {:.4f}%".format(accuracy)
    ]

    print(tabulate([row1, row2, row3], headers=table_headers, numalign="center", tablefmt="fancy_grid"))

def plot_ROC(model, normal_dataset, anomaly_dataset, num_samples=None, verbose=0):
    # Run through the normal and anomalous images to get their results
    if num_samples is not None:
        normal_images_results = get_anomaly_scores(model, normal_dataset, num_samples)
        anomalous_images_results = get_anomaly_scores(model, anomaly_dataset, num_samples)
    else:
        normal_images_results = get_anomaly_scores(model, normal_dataset, normal_dataset.samples)
        anomalous_images_results = get_anomaly_scores(model, anomaly_dataset, anomaly_dataset.samples)

    normal_max = max(normal_images_results)
    normal_min = min(normal_images_results)

    anomaly_max = max(anomalous_images_results)
    anomaly_min = min(anomalous_images_results)

    tprs = []
    fprs = []

    anomaly_score_min = min(anomaly_min, normal_min)
    anomaly_score_max = max(anomaly_max, normal_max)
    anomaly_score_range = anomaly_score_max - anomaly_score_min

    threshold_percentiles = [x/100 for x in range(100)]

    for percentile in threshold_percentiles:
        true_negatives = 0
        false_positives = 0

        true_positives = 0
        false_negatives = 0

        threshold = percentile*anomaly_score_range + anomaly_score_min

        for error in normal_images_results:
            # If that image error was below the threshold (i.e. NOT an anomaly)
            if error < threshold:
                true_negatives += 1
            else:
                false_positives += 1

        for error in anomalous_images_results:
            # If that images error was above the threshold (i.e. An anomaly)
            if error > threshold:
                true_positives += 1
            else:
                false_negatives += 1

        # true positive rate = true positives / (true positives + false negatives)
        tpr = true_positives / (true_positives + false_negatives)

        # false positive rate = false positives / (false positives + true negatives)
        fpr = false_positives / (false_positives + true_negatives)

        # Specificity = true negatives / (true negatives + false positives)
        # false positive rate = 1 - specificity
        specificity = true_negatives / (true_negatives + false_positives)

        tprs.append(tpr)
        fprs.append(fpr)

    # reverse the fprs and tprs lists so that they correspond to starting from (0, 0) instead of from (1, 1)
    fprs.reverse()
    tprs.reverse()

    threshold_percentiles.reverse()

    # Calculate the AUC by calculating the area of all the rectangles and triangles used to create the curve
    AUC = 0.0
    prev_point = (0, 0)
    curr_point = (0, 0)

    for curr_x, curr_y in zip(fprs, tprs):
        # Formula for current points area
        # triangle area = 0.5 * (curr_x - prev_x) * (curr_y - prev_y)
        # rec area = prev_y * (curr_x - prev_x)
        prev_x, prev_y = prev_point

        triangle_area = 0.5 * (curr_x - prev_x) * (curr_y - prev_y)
        rec_area = prev_y * (curr_x - prev_x)

        AUC += triangle_area + rec_area

        prev_point = (curr_x, curr_y)

    if verbose > 0:
        print("AUC ROC = {:.4f}".format(AUC))

        # Save a graph of the training error
        plt.plot(fprs, tprs, color="blue", linestyle="dotted", label="ML Model (AUC = {:.4f})".format(AUC)) 
        plt.plot([0, 1], [0, 1], color="gray", linestyle="--", label="Random model")
        plt.legend()

        plt.xlabel("False positive rate (1 - Specificity)")
        plt.ylabel("True positive rate (Sensitivity)")
        plt.xlim([-0.05, 1.05])
        plt.ylim([-0.05, 1.05])

        plt.savefig(model.image_dir + "ROC.png")

        plt.close()

    # Finding the optimal threshold (find the point on the ROC curve closest to (0, 1))
    # Loop over each point in the graph and use the distance formula to find the point closest to (0, 1)
    distances = []
    point1 = (0, 1)

    for x, y in zip(fprs, tprs):
        point2 = (x, y)
        distances.append(distance_formula(point1, point2))

    min_distance = min(distances)
    min_distance_index = distances.index(min(distances))
    min_threshold = threshold_percentiles[min_distance_index]*anomaly_score_range + anomaly_score_min

    if verbose > 0:
        print("Optimal threshold = {}".format(min_threshold))

    model.error_threshold = min_threshold

    return AUC

# ----------------------------------------------
#            VAE utility functions
# ----------------------------------------------
# 1 norm, sub of absolute values of a vector
def pixel_wise_difference(target, output):
    return tf.reduce_sum(abs(target - output))# / output.shape[0]

# Euclidean norm of (target - output)
# Squared root of the sum of squared vector values
def anomaly_score(target, output):
    return tf.reduce_sum((target - output) ** 2) ** .5

def MSE(target, output):
    return tf.reduce_sum((target - output) ** 2) / output.shape[0]

def log_normal_pdf(sample, mean, logvar, raxis=1):
    log2pi = tf.math.log(2. * np.pi)
    return tf.reduce_sum(-.5 * ((sample - mean) ** 2. * tf.exp(-logvar) + logvar + log2pi), axis=raxis)

# loss_vae = Reconstruction_loss + KL_divergence
def vae_loss_function(model, x):
    mean, logvar = model.encode(x)
    z = model.reparameterize(mean, logvar)
    x_logit = model.decode(z)

    # reconstruction loss (MSE)
    reconstruction_loss = MSE(x, x_logit)

    # KL divergence
    KLD = -0.5 * tf.reduce_mean(1 + logvar - (mean ** 2) - tf.exp(logvar), axis=1)

    return reconstruction_loss + (model.beta * KLD)

def color_of(val):
    if(val == 0.5):
        return "orange"
    
    if(val < 0.5):
        return "red"

    if(val > 0.5):
        return "green"

def generate_and_save_images(model, epoch, images, seed):
    # Generator passes
    g_x = model.generate(images)
    g_seed = model.gen_dec(seed)

    # Discriminator passes
    d_g_x = model.discriminate(g_x)
    d_g_seed = model.discriminate(g_seed)
    d_x = model.discriminate(images)

    # Save the images that were samples
    nun_rows, num_cols = images.shape[0], 6
    figsize = [10, 10]

    fig, ax = plt.subplots(nrows=nun_rows, ncols=num_cols, figsize=figsize)

    for i in range(images.shape[0]):
        # Plot the input image (X)
        ax[i][0].imshow(images[i])
        ax[i][0].axis("off")

        if i == 0:
            ax[i][0].set_title("Input X")

        # Plot D(X)
        ax[i][1].imshow(d_x[i])
        ax[i][1].axis("off")

        if i == 0:
            ax[i][1].set_title("D(X)")

        # Plot G(X)
        ax[i][2].imshow(g_x[i])
        ax[i][2].axis("off")

        if i == 0:
            ax[i][2].set_title("G(X)")

        # Plot D(G(X))
        ax[i][3].imshow(d_g_x[i])
        ax[i][3].axis("off")

        if i == 0:
            ax[i][3].set_title("D(G(X))")

        # Plot seed images
        ax[i][4].imshow(g_seed[i])
        ax[i][4].axis("off")

        if i == 0:
            ax[i][4].set_title("G(seed)")

        # Plot D(G(seed))
        ax[i][5].imshow(d_g_seed[i])
        ax[i][5].axis("off")

        if i == 0:
            ax[i][5].set_title("D(G(seed))")

    plt.savefig(model.image_dir + "image_at_epoch_{:04d}.png".format(epoch))
    plt.close()

def visualiseLatentSpace(model, encoder, dataset, name=None):
    z_vals = []
    z_labels = []

    # Run through the entire dataset
    for i in range(len(dataset)):
        batch, labels = next(dataset)

        # Pass the images through the network
        mean, logvar = tf.split(encoder(batch), num_or_size_splits=2, axis=1)
        z = model.reparameterize(mean, logvar)

        # Save the latent representation and label for each image
        for _z, _l in zip(z, labels):
            z_vals.append(_z.numpy())
            z_labels.append(np.where(_l == 1)[0][0])
            # print(z_labels)

    # Represent the data in 2 components using t-SNE
    # https://scikit-learn.org/stable/modules/generated/sklearn.manifold.TSNE.html#sklearn.manifold.TSNE.fit_transform
    visual_data = TSNE(n_components=2).fit_transform(z_vals)

    # Plot the transformed data
    visual_x = visual_data[:, 0]
    visual_y = visual_data[:, 1]

    # print(z_labels)

    plt.scatter(visual_x, visual_y, c=z_labels, marker=".", cmap=plt.cm.get_cmap("gnuplot", 10))
    plt.colorbar(ticks=range(10))
    plt.clim(-0.5, 9.5)

    if name is not None:
        plt.savefig(model.image_dir + name + ".png")
    else:
        plt.savefig(model.image_dir + "latent_visualisation.png")

    # plt.show()
    plt.close()

#############################
#       Star graphs         #
#############################
# def radar_factory(num_vars, frame='circle'):
#     """Create a radar chart with `num_vars` axes.

#     This function creates a RadarAxes projection and registers it.

#     Parameters
#     ----------
#     num_vars : int
#         Number of variables for radar chart.
#     frame : {'circle' | 'polygon'}
#         Shape of frame surrounding axes.

#     """
#     # calculate evenly-spaced axis angles
#     theta = np.linspace(0, 2*np.pi, num_vars, endpoint=False)

#     class RadarAxes(PolarAxes):

#         name = 'radar'
#         # use 1 line segment to connect specified points
#         RESOLUTION = 1

#         def __init__(self, *args, **kwargs):
#             super().__init__(*args, **kwargs)
#             # rotate plot such that the first axis is at the top
#             self.set_theta_zero_location('N')

#         def fill(self, *args, closed=True, **kwargs):
#             """Override fill so that line is closed by default"""
#             return super().fill(closed=closed, *args, **kwargs)

#         def plot(self, *args, **kwargs):
#             """Override plot so that line is closed by default"""
#             lines = super().plot(*args, **kwargs)
#             for line in lines:
#                 self._close_line(line)

#         def _close_line(self, line):
#             x, y = line.get_data()
#             # FIXME: markers at x[0], y[0] get doubled-up
#             if x[0] != x[-1]:
#                 x = np.concatenate((x, [x[0]]))
#                 y = np.concatenate((y, [y[0]]))
#                 line.set_data(x, y)

#         def set_varlabels(self, labels):
#             self.set_thetagrids(np.degrees(theta), labels)

#         def _gen_axes_patch(self):
#             # The Axes patch must be centered at (0.5, 0.5) and of radius 0.5
#             # in axes coordinates.
#             if frame == 'circle':
#                 return Circle((0.5, 0.5), 0.5)
#             elif frame == 'polygon':
#                 return RegularPolygon((0.5, 0.5), num_vars,
#                                       radius=.5, edgecolor="k")
#             else:
#                 raise ValueError("unknown value for 'frame': %s" % frame)

#         def _gen_axes_spines(self):
#             if frame == 'circle':
#                 return super()._gen_axes_spines()
#             elif frame == 'polygon':
#                 # spine_type must be 'left'/'right'/'top'/'bottom'/'circle'.
#                 spine = Spine(axes=self,
#                               spine_type='circle',
#                               path=Path.unit_regular_polygon(num_vars))
#                 # unit_regular_polygon gives a polygon of radius 1 centered at
#                 # (0, 0) but we want a polygon of radius 0.5 centered at (0.5,
#                 # 0.5) in axes coordinates.
#                 spine.set_transform(Affine2D().scale(.5).translate(.5, .5)
#                                     + self.transAxes)
#                 return {'polar': spine}
#             else:
#                 raise ValueError("unknown value for 'frame': %s" % frame)

#     register_projection(RadarAxes)
#     return theta

# def radarChart(model, normal_dataset, anomaly_dataset):
#     # Generate the latent dimension data
#     z_vals_normal = []
#     z_labels_normal = []

#     # Run through the entire dataset
#     for i in range(len(normal_dataset)):
#         batch, labels = next(normal_dataset)

#         # Pass the images through the network
#         mean, logvar = model.encode(batch)
#         z = model.reparameterize(mean, logvar)

#         # Save the latent representation and label for each image
#         for _z, _l in zip(z, labels):
#             z_vals_normal.append(_z.numpy())
#             z_labels_normal.append(np.where(_l == 1)[0][0]) # Since its a one-hot encoding

#     z_vals_avg = [
#         [0, 0, 0, 0, 0, 0, 0, 0],
#         [0, 0, 0, 0, 0, 0, 0, 0],
#         [0, 0, 0, 0, 0, 0, 0, 0],
#         [0, 0, 0, 0, 0, 0, 0, 0],
#         [0, 0, 0, 0, 0, 0, 0, 0],
#         [0, 0, 0, 0, 0, 0, 0, 0],
#         [0, 0, 0, 0, 0, 0, 0, 0],
#         [0, 0, 0, 0, 0, 0, 0, 0],
#         [0, 0, 0, 0, 0, 0, 0, 0],
#         [0, 0, 0, 0, 0, 0, 0, 0]
#     ]

#     z_vals_avg_count = [0, 0, 0, 0, 0, 0, 0, 0]

#     for z, l in zip(z_vals_normal, z_labels_normal):
#         for i in range(len(z)):
#             z_vals_avg[l][i] += z[i]
#             z_vals_avg_count[i] += 1

#     for i in range(len(z_vals_avg)):
#         for k in range(len(z_vals_avg[i])):
#             if z_vals_avg_count[k] > 0:
#                 z_vals_avg[i][k] /= z_vals_avg_count[k]

#     z_vals_normal = z_vals_normal[:2]
#     z_labels_normal = z_labels_normal[:2]

#     N = model.latent_dim
#     theta = radar_factory(N, frame='polygon')

#     spoke_labels = ["L" + str(i) for i in range(N)]

#     fig, axes = plt.subplots(
#         figsize=(9, 9),
#         nrows=1,
#         ncols=1,
#         subplot_kw=dict(projection='radar')
#     )

#     fig.subplots_adjust(wspace=0.25, hspace=0.20, top=0.85, bottom=0.05)

#     colors = ['r', 'g', 'b', 'c', 'm', 'y', 'k', '#008080', '#00FFFF', '#800000']

#     axes.set_rgrids([0.2, 0.4, 0.6, 0.8])

#     axes.set_title("Test", weight='bold', size='medium', position=(0.5, 1.1), horizontalalignment='center', verticalalignment='center')

#     for d, l in zip(z_vals_avg, z_labels_normal):
#         axes.plot(theta, d, color=colors[l])
#         axes.fill(theta, d, facecolor=colors[l], alpha=0.25)
#         axes.set_varlabels(spoke_labels)

#     labels = [str(i) for i in range(9)]
#     legend = axes.legend(labels, loc=(1.0, .8), labelspacing=0.1, fontsize='small')

#     plt.show()