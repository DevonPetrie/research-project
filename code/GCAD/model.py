# Import tensorflow
import tensorflow as tf
from tensorflow.keras import layers

# Import libraries
import os
import time
import numpy as np
import glob
import matplotlib.pyplot as plt
import PIL
import imageio
import math

import IPython
from IPython import display

from utils import vae_loss_function, generate_and_save_images, pixel_wise_difference, MSE

from architectures import mnist_architecture, cifar10_architecture, crack_architecture

generator_scale = 0.25
discriminator_scale = 0.25

# --------------------------------------------
#                 ADAnomaly
# --------------------------------------------
class ADAnomaly(tf.keras.Model):
    def __init__(self, latent_dim, beta, generatorVAE, discriminatorVAE, image_dir, checkpoint_dir):
        super(ADAnomaly, self).__init__()

        self.image_dir = image_dir
        self.checkpoint_dir = checkpoint_dir

        self.beta = beta

        self.latent_dim = latent_dim
        self.error_threshold = None

        # Number of epochs the training will continue with no increase in validation loss
        self.validation_counter_limit = 5

        # Generator model for the GAN
        self.gen_enc, self.gen_dec = generatorVAE

        # Discriminator model for the GAN
        self.disc_enc, self.disc_dec = discriminatorVAE

    @tf.function
    def train_step(self, train_x, gen_encoder_optimizer, gen_decoder_optimizer, disc_encoder_optimizer, disc_decoder_optimizer):
        seed = tf.random.normal([128, self.latent_dim])

        with tf.GradientTape() as gen_enc_tape, tf.GradientTape() as gen_dec_tape, tf.GradientTape() as disc_enc_tape, tf.GradientTape() as disc_dec_tape:
            # Pass X through the generator
            mean, logvar = tf.split(self.gen_enc(train_x), num_or_size_splits=2, axis=1)
            z_g = self.reparameterize(mean, logvar)

            l_prior_g = -0.5 * tf.reduce_mean(1 + logvar - (mean ** 2) - tf.exp(logvar), axis=1)

            g_x = self.gen_dec(z_g)

            # Create fake images, G(seed), from the seed
            g_seed = self.gen_dec(seed, training=True)

            # Pass X through the discriminator
            mean, logvar = tf.split(self.disc_enc(train_x), num_or_size_splits=2, axis=1)
            z_d_x = self.reparameterize(mean, logvar)

            l_prior_d = -0.5 * tf.reduce_mean(1 + logvar - (mean ** 2) - tf.exp(logvar), axis=1)

            d_x = self.disc_dec(z_d_x)

            # Pass D(X) through the generator
            mean, logvar = tf.split(self.disc_enc(d_x), num_or_size_splits=2, axis=1)
            z_g_d_x = self.reparameterize(mean, logvar)

            g_d_x = self.disc_dec(z_g_d_x)

            # Pass G(X) through the discriminator
            mean, logvar = tf.split(self.disc_enc(g_x), num_or_size_splits=2, axis=1)
            z_d_g_x = self.reparameterize(mean, logvar)

            d_g_x = self.disc_dec(z_d_g_x)

            # Pass G(seed) through the discriminator
            mean, logvar = tf.split(self.disc_enc(g_seed), num_or_size_splits=2, axis=1)
            z_d_g_seed = self.reparameterize(mean, logvar)

            d_g_seed = self.disc_dec(z_d_g_seed)

            # Companion losses
            companion_loss_disc = MSE(g_x, d_g_x)
            companion_loss_gen = MSE(d_x, g_d_x)

            # ====================
            #    Generator VAE
            # ====================
            gen_x_loss = MSE(train_x, g_x)

            # --------------
            #    Encoder
            # --------------
            gen_enc_loss = self.beta * l_prior_g + gen_x_loss + (generator_scale * companion_loss_gen)

            # --------------
            #    Decoder
            # --------------
            # Aim is to minimise the loss of the reproduced images through the discriminator while also minimising the recreation loss of the original image
            # i.e. To produce fake images that seemingly come from the same distribution as the real images
            gen_dec_loss = gen_x_loss + (generator_scale * companion_loss_gen)

            # ====================
            #  Discriminator VAE
            # ====================
            disc_x_loss = MSE(train_x, d_x)

            # --------------
            #    Encoder
            # --------------
            disc_enc_loss = self.beta * l_prior_d + disc_x_loss + (discriminator_scale * companion_loss_disc)

            # --------------
            #    Decoder
            # --------------
            # Aim is to minimise the loss of the original image through the discriminator but maximise the losses of the fake images that come from the generator
            # i.e. To unsuccessfully reproduce fake images while successfully reproducing real images
            disc_dec_loss = disc_x_loss + (discriminator_scale * companion_loss_disc)

        gradients_gen_enc =  gen_enc_tape.gradient(gen_enc_loss, self.gen_enc.trainable_variables)
        gradients_gen_dec =  gen_dec_tape.gradient(gen_dec_loss, self.gen_dec.trainable_variables)

        gradients_disc_enc =  disc_enc_tape.gradient(disc_enc_loss, self.disc_enc.trainable_variables)
        gradients_disc_dec =  disc_dec_tape.gradient(disc_dec_loss, self.disc_dec.trainable_variables)

        gen_encoder_optimizer.apply_gradients(zip(gradients_gen_enc, self.gen_enc.trainable_variables))
        gen_decoder_optimizer.apply_gradients(zip(gradients_gen_dec, self.gen_dec.trainable_variables))
        
        disc_encoder_optimizer.apply_gradients(zip(gradients_disc_enc, self.disc_enc.trainable_variables))
        disc_decoder_optimizer.apply_gradients(zip(gradients_disc_dec, self.disc_dec.trainable_variables))

        return (gen_enc_loss + gen_dec_loss), (disc_enc_loss + disc_dec_loss)

    @tf.function
    def test_step(self, test_x):
        seed = tf.random.normal([128, self.latent_dim])
        
        # Pass X through the generator
        mean, logvar = tf.split(self.gen_enc(test_x), num_or_size_splits=2, axis=1)
        z_g = self.reparameterize(mean, logvar)

        l_prior_g = -0.5 * tf.reduce_mean(1 + logvar - (mean ** 2) - tf.exp(logvar), axis=1)

        g_x = self.gen_dec(z_g)

        # Create fake images, G(seed), from the seed
        g_seed = self.gen_dec(seed, training=True)

        # Pass X through the discriminator
        mean, logvar = tf.split(self.disc_enc(test_x), num_or_size_splits=2, axis=1)
        z_d_x = self.reparameterize(mean, logvar)

        l_prior_d = -0.5 * tf.reduce_mean(1 + logvar - (mean ** 2) - tf.exp(logvar), axis=1)

        d_x = self.disc_dec(z_d_x)

        # Pass D(X) through the generator
        mean, logvar = tf.split(self.disc_enc(d_x), num_or_size_splits=2, axis=1)
        z_g_d_x = self.reparameterize(mean, logvar)

        g_d_x = self.disc_dec(z_g_d_x)

        # Pass G(X) through the discriminator
        mean, logvar = tf.split(self.disc_enc(g_x), num_or_size_splits=2, axis=1)
        z_d_g_x = self.reparameterize(mean, logvar)

        d_g_x = self.disc_dec(z_d_g_x)

        # Pass G(seed) through the discriminator
        mean, logvar = tf.split(self.disc_enc(g_seed), num_or_size_splits=2, axis=1)
        z_d_g_seed = self.reparameterize(mean, logvar)

        d_g_seed = self.disc_dec(z_d_g_seed)

        # Companion losses
        companion_loss_disc = MSE(g_x, d_g_x)
        companion_loss_gen = MSE(d_x, g_d_x)

        # ====================
        #    Generator VAE
        # ====================
        gen_x_loss = MSE(test_x, g_x)

        # --------------
        #    Encoder
        # --------------
        gen_enc_loss = self.beta * l_prior_g + gen_x_loss + (generator_scale * companion_loss_gen)

        # --------------
        #    Decoder
        # --------------
        # Aim is to minimise the loss of the reproduced images through the discriminator while also minimising the recreation loss of the original image
        # i.e. To produce fake images that seemingly come from the same distribution as the real images
        gen_dec_loss = gen_x_loss + (generator_scale * companion_loss_gen)

        # ====================
        #  Discriminator VAE
        # ====================
        disc_x_loss = MSE(test_x, d_x)

        # --------------
        #    Encoder
        # --------------
        disc_enc_loss = self.beta * l_prior_d + disc_x_loss + (discriminator_scale * companion_loss_disc)

        # --------------
        #    Decoder
        # --------------
        # Aim is to minimise the loss of the original image through the discriminator but maximise the losses of the fake images that come from the generator
        # i.e. To unsuccessfully reproduce fake images while successfully reproducing real images
        disc_dec_loss = disc_x_loss + (discriminator_scale * companion_loss_disc)

        return (gen_enc_loss + gen_dec_loss), (disc_enc_loss + disc_dec_loss)

    def train(self, train_dataset, validation_dataset, epochs, learning_rate_gen, learning_rate_disc, optimizer, verbose=0):
        if optimizer == "adam":
            gen_encoder_optimizer = tf.keras.optimizers.Adam(learning_rate_gen)
            gen_decoder_optimizer = tf.keras.optimizers.Adam(learning_rate_gen)
            disc_encoder_optimizer = tf.keras.optimizers.Adam(learning_rate_disc)
            disc_decoder_optimizer = tf.keras.optimizers.Adam(learning_rate_disc)
        elif optimizer == "rmsprop":
            gen_encoder_optimizer = tf.keras.optimizers.RMSprop(learning_rate_gen)
            gen_decoder_optimizer = tf.keras.optimizers.RMSprop(learning_rate_gen)
            disc_encoder_optimizer = tf.keras.optimizers.RMSprop(learning_rate_disc)
            disc_decoder_optimizer = tf.keras.optimizers.RMSprop(learning_rate_disc)

        checkpoint_dir = os.path.dirname(self.checkpoint_dir)

        # Get the first 6 images and use them as predictions each epoch to see improvement
        self.imagesToGenerate = next(validation_dataset)[0][0:6]

        self.random_vector_for_generation = tf.random.normal(shape=(6, self.latent_dim))

        if verbose > 0:
            generate_and_save_images(self, 0, self.imagesToGenerate, self.random_vector_for_generation)

        # Init for stats saving
        graphs_x = []
        error_graph_train_gen = []
        error_graph_train_disc = []

        error_graph_validation_gen = []
        error_graph_validation_disc = []

        # Init for early stopping variables
        validation_best_counter = 0
        best_validation_loss = None
        best_train_loss = None

        # Training loop
        for epoch in range(1, epochs + 1):
            # Training step
            start_time = time.time()

            length = len(train_dataset)

            for i in range(length):
                train_x, _ = next(train_dataset)

                self.train_step(train_x, gen_encoder_optimizer, gen_decoder_optimizer, disc_encoder_optimizer, disc_decoder_optimizer)

                # print("\r{}/{} = {:.2f}%".format(i, length, (i/length)*100), end="", flush=True)

            # print("\nDone with training loop for epoch {}".format(epoch))
            end_time = time.time()

            if epoch % 1 == 0:
                # Statistics step
                # Train set loss
                train_loss_gen = tf.keras.metrics.Mean()
                train_loss_disc = tf.keras.metrics.Mean()

                length = len(train_dataset)

                for i in range(length):
                    train_x, _ = next(train_dataset)

                    gen_loss, disc_loss = self.test_step(train_x)
                    
                    train_loss_gen(gen_loss)
                    train_loss_disc(disc_loss)

                    # print("\r{}/{} = {:.2f}%".format(i, length, (i/length)*100), end="", flush=True)

                # print("")
                train_loss_gen = train_loss_gen.result()
                train_loss_disc = train_loss_disc.result()

                # Validation set loss
                validation_loss_gen = tf.keras.metrics.Mean()
                validation_loss_disc = tf.keras.metrics.Mean()

                length = len(validation_dataset)

                for i in range(length):
                    val_x, _ = next(validation_dataset)

                    gen_loss, disc_loss = self.test_step(val_x)

                    validation_loss_gen(gen_loss)
                    validation_loss_disc(disc_loss)

                    # print("\r{}/{} = {:.2f}%".format(i, length, (i/length)*100), end="", flush=True)

                # print("")
                validation_loss_gen = validation_loss_gen.result()
                validation_loss_disc = validation_loss_disc.result()

                # Save stats
                graphs_x.append(epoch)

                error_graph_train_gen.append(train_loss_gen)
                error_graph_train_disc.append(train_loss_disc)

                error_graph_validation_gen.append(validation_loss_gen)
                error_graph_validation_disc.append(validation_loss_disc)

                display.clear_output(wait=False)

                # Validation set for early stopping
                if best_validation_loss == None or (validation_loss_gen + validation_loss_disc) < best_validation_loss:
                    validation_best_counter = 0
                    best_validation_loss = (validation_loss_gen + validation_loss_disc)

                    best_train_loss = (train_loss_gen + train_loss_disc)

                    if verbose > 0:
                        print("Saving weights to {}weights_last".format(self.checkpoint_dir))

                        self.save_weights("{}{}".format(self.checkpoint_dir, "weights_last"))

                if validation_best_counter >= self.validation_counter_limit:
                    if verbose > 0:
                        print("Early stopping! Generalisation error hasn't increased in {} epochs.".format(self.validation_counter_limit))

                    break

                validation_best_counter += 1

                if verbose > 0:
                    generate_and_save_images(self, epoch, self.imagesToGenerate, self.random_vector_for_generation)

                    print("Epoch: {}, Generator validation set loss = {}, Discriminator validation set loss = {}, time elapsed for current epoch: {} seconds".format(
                        epoch, validation_loss_gen, validation_loss_disc, end_time - start_time
                    ))

        if verbose > 0:
            # Generate gif of training
            anim_file = self.image_dir + "ADAnomaly_training.gif"

            with imageio.get_writer(anim_file, mode="I") as writer:
                filenames = glob.glob(self.image_dir + "image*.png")
                filenames = sorted(filenames)
                last = -1
                for i, filename in enumerate(filenames):
                    frame = 2*(i**0.5)
                    if round(frame) > round(last):
                        last = frame
                    else:
                        continue

                    image = imageio.imread(filename)
                    writer.append_data(image)
                image = imageio.imread(filename)
                writer.append_data(image)

            if IPython.version_info >= (6,2,0,""):
                display.Image(filename=anim_file)

            # Save a graph of the training error
            plt.plot(graphs_x, error_graph_validation_gen, color="green", label="Generator")
            plt.plot(graphs_x, error_graph_validation_disc, color="blue", label="Discriminator")
            plt.legend()

            plt.ylabel("Validation set loss")
            plt.xlabel("Epoch")
            plt.savefig(self.image_dir + "training_graph.png")

            plt.close()

            print("\n-------------------------")
            print("       End losses        ")
            print("-------------------------")
            print("Train loss: {:.4f}".format(best_train_loss))
            print("Validation loss: {:.4f}".format(best_validation_loss))

        return best_validation_loss

    @tf.function
    def sample(self, epsilon=None):
        if epsilon is None:
            epsilon = tf.random.normal(shape=(100, self.latent_dim))
        return self.decode(epsilon)

    def encode(self, x):
        mean, logvar = tf.split(self.encoder(x), num_or_size_splits=2, axis=1)

        return mean, logvar

    def reparameterize(self, mean, logvar):
        epsilon = tf.random.normal(shape=mean.shape)
        return mean + (epsilon * tf.exp(logvar * .5))

    def decode(self, z, apply_sigmoid=False):
        logits = self.decoder(z)
        if apply_sigmoid:
            probs = tf.sigmoid(logits)
            return probs

        return logits

    def call(self, x):
        # x_tilde = self.discriminate(x)

        # return x_tilde

        g_x = self.generate(x)
        d_g_x = self.discriminate(g_x)

        return d_g_x

    def generate(self, inputs):
        mean, logvar = tf.split(self.gen_enc(inputs), num_or_size_splits=2, axis=1)
        z = self.reparameterize(mean, logvar)

        return self.gen_dec(z)

    def discriminate(self, inputs):
        mean, logvar = tf.split(self.disc_enc(inputs), num_or_size_splits=2, axis=1)
        z = self.reparameterize(mean, logvar)

        return self.disc_dec(z)

    def clear_model(self):
        tf.keras.backend.clear_session()