for i in 0 1 2 3 4 5 6 7 8 9
do
    cd datasets/mnist/test/$i/
    ls | xargs -I {} mv {} a_{}

    cd ../../train/$i/
    ls | xargs -I {} mv {} b_{}

    cd ../../../..
done