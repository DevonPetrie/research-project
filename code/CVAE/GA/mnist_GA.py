# GENETIC ALGORITHM!!!! :D
# YAAAYYYYY!
from model import CVAE

from random import seed, randint, choice

from datasets import mnist_dataset

seed()

import matplotlib.pyplot as plt
import tensorflow as tf

import math

import utils

def encoder_decoder(layers, filters_per_layer, kernel_size, latent_dim):
    if layers == 1:
        encoder = tf.keras.Sequential(
        [
            tf.keras.layers.InputLayer(input_shape=(28, 28, 3)),
            tf.keras.layers.Conv2D(
                filters=filters_per_layer[0],
                kernel_size=kernel_size,
                strides=(2, 2),
                padding="same",
                activation="relu"
            ),
            tf.keras.layers.Flatten(),
            # No activation
            tf.keras.layers.Dense(latent_dim + latent_dim),
        ])

        # DeConv structure for the decoder net
        decoder = tf.keras.Sequential(
        [
            tf.keras.layers.InputLayer(input_shape=(latent_dim,)),
            tf.keras.layers.Dense(units=7*7*28, activation=tf.nn.relu),
            tf.keras.layers.Reshape(target_shape=(7, 7, 28)),
            tf.keras.layers.Conv2DTranspose(
                filters=filters_per_layer[0],
                kernel_size=kernel_size,
                strides=(2, 2),
                padding="same",
                activation="relu"
            ),
            # No activation
            tf.keras.layers.Conv2DTranspose(filters=3, kernel_size=kernel_size, strides=(2, 2), padding="same", activation="sigmoid"),
        ])
    elif layers == 2:
        encoder = tf.keras.Sequential(
        [
            tf.keras.layers.InputLayer(input_shape=(28, 28, 3)),
            tf.keras.layers.Conv2D(
                filters=filters_per_layer[0],
                kernel_size=kernel_size,
                strides=(2, 2),
                padding="same",
                activation="relu"
            ),
            tf.keras.layers.Conv2D(
                filters=filters_per_layer[1],
                kernel_size=kernel_size,
                strides=(2, 2),
                padding="same",
                activation="relu"
            ),
            tf.keras.layers.Flatten(),
            # No activation
            tf.keras.layers.Dense(latent_dim + latent_dim),
        ])

        # DeConv structure for the decoder net
        decoder = tf.keras.Sequential(
        [
            tf.keras.layers.InputLayer(input_shape=(latent_dim,)),
            tf.keras.layers.Dense(units=7*7*28, activation=tf.nn.relu),
            tf.keras.layers.Reshape(target_shape=(7, 7, 28)),
            tf.keras.layers.Conv2DTranspose(
                filters=filters_per_layer[1],
                kernel_size=kernel_size,
                strides=(2, 2),
                padding="same",
                activation="relu"
            ),
            tf.keras.layers.Conv2DTranspose(
                filters=filters_per_layer[0],
                kernel_size=kernel_size,
                strides=(2, 2),
                padding="same",
                activation="relu"
            ),
            # No activation
            tf.keras.layers.Conv2DTranspose(filters=3, kernel_size=kernel_size, strides=(1, 1), padding="same", activation="sigmoid"),
        ])
    elif layers == 3:
        encoder = tf.keras.Sequential(
        [
            tf.keras.layers.InputLayer(input_shape=(28, 28, 3)),
            tf.keras.layers.Conv2D(
                filters=filters_per_layer[0],
                kernel_size=kernel_size,
                strides=(2, 2),
                padding="same",
                activation="relu"
            ),
            tf.keras.layers.Conv2D(
                filters=filters_per_layer[1],
                kernel_size=kernel_size,
                strides=(1, 1),
                padding="same",
                activation="relu"
            ),
            tf.keras.layers.Conv2D(
                filters=filters_per_layer[2],
                kernel_size=kernel_size,
                strides=(2, 2),
                padding="same",
                activation="relu"
            ),
            tf.keras.layers.Flatten(),
            # No activation
            tf.keras.layers.Dense(latent_dim + latent_dim),
        ])

        # DeConv structure for the decoder net
        decoder = tf.keras.Sequential(
        [
            tf.keras.layers.InputLayer(input_shape=(latent_dim,)),
            tf.keras.layers.Dense(units=7*7*28, activation=tf.nn.relu),
            tf.keras.layers.Reshape(target_shape=(7, 7, 28)),
            tf.keras.layers.Conv2DTranspose(
                filters=filters_per_layer[2],
                kernel_size=kernel_size,
                strides=(2, 2),
                padding="same",
                activation="relu"
            ),
            tf.keras.layers.Conv2DTranspose(
                filters=filters_per_layer[1],
                kernel_size=kernel_size,
                strides=(1, 1),
                padding="same",
                activation="relu"
            ),
            tf.keras.layers.Conv2DTranspose(
                filters=filters_per_layer[0],
                kernel_size=kernel_size,
                strides=(2, 2),
                padding="same",
                activation="relu"
            ),
            # No activation
            tf.keras.layers.Conv2DTranspose(filters=3, kernel_size=kernel_size, strides=(1, 1), padding="same", activation="sigmoid"),
        ])

    return encoder, decoder

def hyperparameters():
    layers = [1, 2, 3]

    filters_per_layer = [16, 32, 64, 128, 256]

    optimizer = ["adam", "rmsprop"]

    learning_rate = [5e-4, 1e-4, 5e-5, 1e-5]

    kernel_size = [2, 3, 4, 5]

    latent_dim = [8, 16, 32, 64]

    return [layers, filters_per_layer, optimizer, learning_rate, kernel_size, latent_dim]

class Chromosome():
    def __init__(self, params=None):
        if params is not None:
            self.layers = params[0]
            self.filters_per_layer = params[1]
            self.optimizer = params[2]
            self.learning_rate = params[3]
            self.kernel_size = params[4]
            self.latent_dim = params[5]
        else:
            # Generate random hyperparameters
            parameters = hyperparameters()

            self.layers = choice(parameters[0])

            self.filters_per_layer = []
            for i in range(self.layers):
                self.filters_per_layer.append(choice(parameters[1]))
            self.filters_per_layer.sort(reverse=True)

            self.optimizer = choice(parameters[2])
            self.learning_rate = choice(parameters[3])
            self.kernel_size = choice(parameters[4])
            self.latent_dim = choice(parameters[5])

        # Convolutional structure for the encoder net
        self.encoder, self.decoder = encoder_decoder(self.layers, self.filters_per_layer, self.kernel_size, self.latent_dim)

        self.params = [self.layers, self.filters_per_layer, self.optimizer, self.learning_rate, self.kernel_size, self.latent_dim]

        self.fitness_value = None

    # Fitness value in this algorithm needs to be maximised
    def fitness(self):
        labels = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]

        test_classes = ["1", "2", "9"]

        sum_roc_auc = 0

        target_size = (28, 28)
        BATCH_SIZE = 128

        for k in range(len(test_classes)):
            # Create the model
            model = CVAE(self.latent_dim, "", "")

            model.encoder = self.encoder
            model.decoder = self.decoder

            epochs = 20

            anomaly_classes = [test_classes[k]]
            normal_classes = [x for x in labels if x not in anomaly_classes]

            (train_dataset, validation_dataset), (test_normal_dataset, test_anomaly_dataset, test_total_dataset) = mnist_dataset.load(
                batch_size=BATCH_SIZE,
                target_size=target_size,
                classes=labels,
                normal_classes=normal_classes,
                anomaly_classes=anomaly_classes
            )

            with tf.device("/GPU:0"):
                try:
                    val_loss = model.train(train_dataset, validation_dataset, epochs, self.learning_rate, self.optimizer)#, verbose=1)

                    if math.isnan(val_loss):
                        sum_roc_auc += -1
                        break
                    else:
                        sum_roc_auc += utils.plot_ROC(model, test_normal_dataset, test_anomaly_dataset)
                except KeyboardInterrupt:
                    print(" received, stopped training...")

            model.clear_model()

        self.fitness_value = sum_roc_auc/len(test_classes)

        return self.fitness_value

class GA():
    def generate_population(self, popSize):
        population = []

        i = 0
        while i < popSize:
            chromosome = Chromosome()

            if not self.chromosomeExists(population, chromosome):
                population.append(chromosome)
                i += 1

        return population

    def chromosomeExists(self, arrayOfChromosomes, chrom):
        for x in arrayOfChromosomes:
            if x.params == chrom.params:
                return True
        return False

    def children_population(self, size, population):
        child_population = []

        population_size = len(population)

        fitnesses = [population[i].fitness_value for i in range(population_size)]

        # Indices, in order of top performing (i.e. highest fitness value)
        sorted_population = sorted(range(population_size), key=lambda i: fitnesses[i], reverse=True)

        # ------------------------------
        #    Reproduction, top 10%
        # ------------------------------
        reproductionNum = math.ceil(population_size*0.1)

        reproduction = sorted_population[:reproductionNum]
        reproduction = [population[x] for x in reproduction]

        child_population += reproduction

        # ------------------------------
        #      crossover, top 50%
        # ------------------------------
        if population_size <= 2:
            crossoverNum = 2
        else:
            crossoverNum = math.ceil(population_size*0.5)

        crossoverSelection = sorted_population[:crossoverNum]
        crossoverChildren = []

        parent1Index = 0
        parent2Index = 0

        failedCreatingUnique = 0

        while len(crossoverChildren) < len(crossoverSelection):
            parentA = population[crossoverSelection[parentIndex%len(crossoverSelection)]]
            parentB = population[crossoverSelection[(parentIndex+1)%len(crossoverSelection)]]

            childA, childB = self.crossover(a=parentA, b=parentB)

            crossoverChildren.append(childA)
            crossoverChildren.append(childB)

        child_population += crossoverChildren

        # ------------------------------
        #    Mutate the rest, top 40%
        # ------------------------------
        mutateNum = population_size - reproductionNum - crossoverNum

        mutation_pop = sorted_population[:mutateNum]

        mutation_pop = [mutation(population[x], 0.6) for x in mutation_pop]

        child_population += mutation_pop

        return child_population

    def crossover(self, a, b):
        parameters = hyperparameters()

        crossover_point = choice(range(2, len(parameters)))

        new_paramsA = []
        new_paramsB = []

        for i in range(len(parameters)):
            if i < crossover_point:
                new_paramsA.append(a.params[i])
                new_paramsB.append(b.params[i])
            else:
                new_paramsA.append(b.params[i])
                new_paramsB.append(a.params[i])

        return Chromosome(params=new_paramsA), Chromosome(params=new_paramsB)

    def mutation(self, a, mutation_rate):
        parameters = hyperparameters()

        new_params = []

        # Handle parameters 0 and 1 seperately to the rest
        if (randint(0, 100) / 100) < mutation_rate: # Mutate both parameters 0 and 1
            new_params.append(choice(parameters[0]))

            filters_per_layer = []
            for i in range(new_params[0]):
                filters_per_layer.append(choice(parameters[1]))

            filters_per_layer.sort(reverse=True)

            new_params.append(filters_per_layer)
        else:
            new_params.append(parameters[0])
            new_params.append(parameters[1])

        for i in range(2, len(parameters)):
            if (randint(0, 100) / 100) < mutation_rate: # MUTATE
                new_params.append(choice(parameters[i]))
            else:
                new_params.append(parameters[i])

        return Chromosome(params=new_params)

    def population_fitnesses(self, population):
        fitnesses = []

        popSize = len(population)

        loadBar = ">"
        nextBoundary = 2
        width = 50

        for i in range(popSize):
            fitnesses.append(population[i].fitness())

            # Loading bar stuff
            percentDone = int(((i+1)/popSize) * 100)

            if percentDone >= nextBoundary:
                loadBar = "="*int((percentDone/100)*width) + ">"
                nextBoundary += 2

            print("{}% [{}{}] {} - {} - fitness = {:.4f}".format(percentDone, loadBar, " " * (width-len(loadBar)), i+1, population[i].params, fitnesses[len(fitnesses)-1]))

        return fitnesses

    def deduceArchitecture(self, popSize, generations):
        # ------------------------------
        #      Initial population
        # ------------------------------
        population = self.generate_population(popSize)

        architecture_fitness_stats = []

        architecture_ind = []

        best_fitness = 0.0
        best_fitness_counter = 0
        best_fitness_counter_limit = 3

        for gen in range(generations):
            # ------------------------------
            #     Calculate fitnesses
            # ------------------------------
            # expensive step bois and gals
            print("Calculating individuals fitnesses...")

            fitnesses = self.population_fitnesses(population)

            # Save populations max fitness
            architecture_fitness_stats.append(max([population[x].fitness_value for x in range(len(population))]))
            architecture_ind.append(gen)

            # ------------------------------
            #         Get winners
            # ------------------------------
            topPerformance = max(fitnesses)
            topPerformer = population[fitnesses.index(topPerformance)]

            print("Generation {} top performing individual -> {}, fitness = {:.4f}".format(gen, topPerformer.params, topPerformance))

            # Early stopping if top performer doesn't increase for N generations
            if topPerformance > best_fitness:
                best_fitness_counter = 0
                best_fitness = topPerformance
            else:
                best_fitness_counter += 1

            if best_fitness_counter >= best_fitness_counter_limit:
                print("Top performers fitness hasn't increased in {} generations, termination criteria.".format(best_fitness_counter_limit))
                break

            # ------------------------------
            #   Generate next population 
            #   with genetic operations
            # ------------------------------
            population = self.children_population(popSize, population)

        plt.plot(architecture_ind, architecture_fitness_stats, color="blue", label="Generation maximum fitness")
        plt.legend()
        plt.ylabel("Fitness (AUC ROC)")
        plt.xlabel("Generation")
        plt.savefig("./architecture_fitness_stats.png")

        plt.close()