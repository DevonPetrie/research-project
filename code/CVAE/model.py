# Import tensorflow
import tensorflow as tf

# Import libraries
import os
import time
import numpy as np
import glob
import matplotlib.pyplot as plt
import PIL
import imageio
import math

import IPython
from IPython import display

from utils import loss_function, generate_and_save_images

# --------------------------------------------
#    Convolutional variational autoencoder
# --------------------------------------------
class CVAE(tf.keras.Model):
    def __init__(self, latent_dim, image_dir, checkpoint_dir):
        super(CVAE, self).__init__()

        self.image_dir = image_dir
        self.checkpoint_dir = checkpoint_dir

        self.latent_dim = latent_dim
        self.error_threshold = None

        # Number of epochs the training will continue with no increase in validation loss
        self.validation_counter_limit = 5

        # Convolutional structure for the encoder net
        self.encoder = tf.keras.Sequential(
        [
            tf.keras.layers.InputLayer(input_shape=(32, 32, 3)),
            tf.keras.layers.Conv2D(
                filters=512,
                kernel_size=2,
                strides=(2, 2),
                padding="same",
                activation="relu"
            ),
            tf.keras.layers.Conv2D(
                filters=32,
                kernel_size=2,
                strides=(1, 1),
                padding="same",
                activation="relu"
            ),
            tf.keras.layers.Conv2D(
                filters=16,
                kernel_size=2,
                strides=(2, 2),
                padding="same",
                activation="relu"
            ),
            tf.keras.layers.Flatten(),
            # No activation
            tf.keras.layers.Dense(latent_dim + latent_dim),
        ])

        # DeConv structure for the decoder net
        self.decoder = tf.keras.Sequential(
        [
            tf.keras.layers.InputLayer(input_shape=(latent_dim,)),
            tf.keras.layers.Dense(units=8*8*32, activation=tf.nn.relu),
            tf.keras.layers.Reshape(target_shape=(8, 8, 32)),
            tf.keras.layers.Conv2DTranspose(
                filters=16,
                kernel_size=2,
                strides=(2, 2),
                padding="same",
                activation="relu"
            ),
            tf.keras.layers.Conv2DTranspose(
                filters=32,
                kernel_size=2,
                strides=(1, 1),
                padding="same",
                activation="relu"
            ),
            tf.keras.layers.Conv2DTranspose(
                filters=512,
                kernel_size=2,
                strides=(2, 2),
                padding="same",
                activation="relu"
            ),
            tf.keras.layers.Conv2DTranspose(filters=3, kernel_size=2, strides=(1, 1), padding="same", activation="sigmoid"),
        ])

    @tf.function
    def train_step(self, train_x, optimizer):
        with tf.GradientTape() as tape:
            loss = loss_function(self, train_x)

        gradients = tape.gradient(loss, self.trainable_variables)
        optimizer.apply_gradients(zip(gradients, self.trainable_variables))

    @tf.function
    def test_step(self, test_x):
        return loss_function(self, test_x)

    def train(self, train_dataset, validation_dataset, epochs, learning_rate, optimizer, verbose=0):
        if optimizer == "adam":
            optimizer = tf.keras.optimizers.Adam(learning_rate)
        elif optimizer == "rmsprop":
            optimizer = tf.keras.optimizers.RMSprop(learning_rate)

        checkpoint_dir = os.path.dirname(self.checkpoint_dir)

        # Get the first 16 images and use them as predictions each epoch to see improvement
        self.imagesToGenerate = next(validation_dataset)[0][0:16]

        if verbose > 0:
            plt.figure(figsize=(4, 4))
            for i in range(16):
                ax = plt.subplot(4, 4, i + 1)
                plt.imshow(self.imagesToGenerate[i])
                plt.axis("off")

            plt.savefig(self.image_dir + "targets.png")
            plt.close()

            generate_and_save_images(self, 0, self.imagesToGenerate)

        # Init for stats saving
        graphs_x = []
        error_graph_train = []
        error_graph_validation = []

        # Init for early stopping variables
        validation_best_counter = 0
        best_validation_loss = None
        best_train_loss = None

        # Training loop
        for epoch in range(1, epochs + 1):
            # Training step
            start_time = time.time()

            length = len(train_dataset)

            for i in range(length):
                train_x, _ = next(train_dataset)
                self.train_step(train_x, optimizer)

                # print("\r{}/{} = {:.2f}%".format(i, length, (i/length)*100), end="", flush=True)

            # print("\nDone with training loop for epoch {}".format(epoch))
            end_time = time.time()

            if epoch % 1 == 0:
                # Statistics step
                # Train set loss
                train_loss = tf.keras.metrics.Mean()

                length = len(train_dataset)

                for i in range(length):
                    train_x, _ = next(train_dataset)
                    train_loss(self.test_step(train_x))

                    # print("\r{}/{} = {:.2f}%".format(i, length, (i/length)*100), end="", flush=True)

                # print("")
                train_loss = train_loss.result()

                # Validation set loss
                validation_loss = tf.keras.metrics.Mean()

                length = len(validation_dataset)

                for i in range(length):
                    val_x, _ = next(validation_dataset)
                    validation_loss(self.test_step(val_x))

                    # print("\r{}/{} = {:.2f}%".format(i, length, (i/length)*100), end="", flush=True)

                # print("")
                validation_loss = validation_loss.result()

                if math.isnan(train_loss) or math.isnan(validation_loss):
                    return float('NaN')

                # Save stats
                graphs_x.append(epoch)

                error_graph_train.append(train_loss)
                error_graph_validation.append(validation_loss)

                display.clear_output(wait=False)

                # Validation set for early stopping
                if best_validation_loss == None or validation_loss < best_validation_loss:
                    validation_best_counter = 0
                    best_validation_loss = validation_loss

                    best_train_loss = train_loss

                    if verbose > 0:
                        print("Saving weights to {}weights_last".format(self.checkpoint_dir))

                        self.save_weights("{}{}".format(self.checkpoint_dir, "weights_last"))

                        generate_and_save_images(self, epoch, self.imagesToGenerate)

                if validation_best_counter >= self.validation_counter_limit:
                    if verbose > 0:
                        print("Early stopping! Generalisation error hasn't increased in {} epochs.".format(self.validation_counter_limit))

                    break

                validation_best_counter += 1

                if verbose > 0:
                    print("Epoch: {}, Train set loss = {}, Validation set loss = {}, time elapse for current epoch: {} seconds".format(
                        epoch, train_loss, validation_loss, end_time - start_time
                    ))

        if verbose > 0:
            # Generate gif of training
            anim_file = self.image_dir + "cvae_training.gif"

            with imageio.get_writer(anim_file, mode="I") as writer:
                filenames = glob.glob(self.image_dir + "image*.png")
                filenames = sorted(filenames)
                last = -1
                for i, filename in enumerate(filenames):
                    frame = 2*(i**0.5)
                    if round(frame) > round(last):
                        last = frame
                    else:
                        continue

                    image = imageio.imread(filename)
                    writer.append_data(image)
                image = imageio.imread(filename)
                writer.append_data(image)

            if IPython.version_info >= (6,2,0,""):
                display.Image(filename=anim_file)

            # Save a graph of the training error
            plt.plot(graphs_x, error_graph_train, color="green", label="Train set loss")
            plt.plot(graphs_x, error_graph_validation, color="blue", label="Validation set loss")
            plt.legend()

            plt.ylabel("Error (loss)")
            plt.xlabel("Epoch")
            plt.savefig(self.image_dir + "training_graph.png")

            plt.close()

            print("\n-------------------------")
            print("       End losses        ")
            print("-------------------------")
            print("Train loss: {:.4f}".format(best_train_loss))
            print("Validation loss: {:.4f}".format(best_validation_loss))

        return best_validation_loss

    @tf.function
    def sample(self, epsilon=None):
        if epsilon is None:
            epsilon = tf.random.normal(shape=(100, self.latent_dim))
        return self.decode(epsilon)

    def encode(self, x):
        mean, logvar = tf.split(self.encoder(x), num_or_size_splits=2, axis=1)

        return mean, logvar

    def reparameterize(self, mean, logvar):
        epsilon = tf.random.normal(shape=mean.shape)
        return mean + (epsilon * tf.exp(logvar * .5))

    def decode(self, z, apply_sigmoid=False):
        logits = self.decoder(z)
        if apply_sigmoid:
            probs = tf.sigmoid(logits)
            return probs

        return logits

    def call(self, x):
        mean, logvar = self.encode(x)
        z = self.reparameterize(mean, logvar)

        return self.sample(z)

    def clear_model(self):
        tf.keras.backend.clear_session()
