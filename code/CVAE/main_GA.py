from GA.mnist_GA import GA as GA_mnist
from GA.cifar10_GA import GA as GA_cifar10
from GA.crack_GA import GA as GA_crack
from GA.blackhole_GA import GA as GA_black_holes

if __name__ == "__main__":
    # print("----------------------------------")
    # print("  Genetic algorithm for MNIST")
    # print("----------------------------------")
    # geneticAlgorithm = GA_mnist()

    # geneticAlgorithm.deduceArchitecture(
    #     popSize=9,
    #     generations=3
    # )

    print("\n\n----------------------------------")
    print("  Genetic algorithm for CIFAR-10")
    print("----------------------------------")
    geneticAlgorithm = GA_cifar10()

    geneticAlgorithm.deduceArchitecture(
        popSize=9,
        generations=3
    )

    # print("\n\n----------------------------------")
    # print("   Genetic algorithm for crack    ")
    # print("----------------------------------")
    # geneticAlgorithm = GA_crack()

    # geneticAlgorithm.deduceArchitecture(
    #     popSize=6,
    #     generations=3
    # )

    # print("\n\n----------------------------------")
    # print(" Genetic algorithm for black-holes")
    # print("----------------------------------")
    # geneticAlgorithm = GA_black_holes()

    # geneticAlgorithm.deduceArchitecture(
    #     popSize=6,
    #     generations=3
    # )