import tensorflow as tf
import os

import numpy as np
import matplotlib.pyplot as plt

# to generate random integer values
from random import seed, randint

seed(1)

from model import betaCVAE

from architectures import mnist_architecture, cifar10_architecture, crack_architecture, black_holes_architecture

from datasets import mnist_dataset, crack_dataset, cifar10_dataset, black_hole_dataset

import utils

# ----------------------------------------------
#              User interaction
# ----------------------------------------------
def printMenu(clearScreen = False):
    # Red - "\033[31m"
    # Blue - "\033[36m"
    # White - "\033[37m"

    # Font Name: Bloody
    logo = ""
    # logo += " ▄▄▄       █    ██ ▄▄▄█████▓ ▒█████  ▓█████  ███▄    █  ▄████▄   ▒█████  ▓█████▄ ▓█████  ██▀███  \n"
    # logo += "▒████▄     ██  ▓██▒▓  ██▒ ▓▒▒██▒  ██▒▓█   ▀  ██ ▀█   █ ▒██▀ ▀█  ▒██▒  ██▒▒██▀ ██▌▓█   ▀ ▓██ ▒ ██▒\n"
    # logo += "▒██  ▀█▄  ▓██  ▒██░▒ ▓██░ ▒░▒██░  ██▒▒███   ▓██  ▀█ ██▒▒▓█    ▄ ▒██░  ██▒░██   █▌▒███   ▓██ ░▄█ ▒\n"
    # logo += "░██▄▄▄▄██ ▓▓█  ░██░░ ▓██▓ ░ ▒██   ██░▒▓█  ▄ ▓██▒  ▐▌██▒▒▓▓▄ ▄██▒▒██   ██░░▓█▄   ▌▒▓█  ▄ ▒██▀▀█▄  \n"
    # logo += " ▓█   ▓██▒▒▒█████▓   ▒██▒ ░ ░ ████▓▒░░▒████▒▒██░   ▓██░▒ ▓███▀ ░░ ████▓▒░░▒████▓ ░▒████▒░██▓ ▒██▒\n"
    # logo += " ▒▒   ▓▒█░░▒▓▒ ▒ ▒   ▒ ░░   ░ ▒░▒░▒░ ░░ ▒░ ░░ ▒░   ▒ ▒ ░ ░▒ ▒  ░░ ▒░▒░▒░  ▒▒▓  ▒ ░░ ▒░ ░░ ▒▓ ░▒▓░\n"
    # logo += "  ▒   ▒▒ ░░░▒░ ░ ░     ░      ░ ▒ ▒░  ░ ░  ░░ ░░   ░ ▒░  ░  ▒     ░ ▒ ▒░  ░ ▒  ▒  ░ ░  ░  ░▒ ░ ▒░\n"
    # logo += "  ░   ▒    ░░░ ░ ░   ░      ░ ░ ░ ▒     ░      ░   ░ ░ ░        ░ ░ ░ ▒   ░ ░  ░    ░     ░░   ░ \n"
    # logo += "      ░  ░   ░                  ░ ░     ░  ░         ░ ░ ░          ░ ░     ░       ░  ░   ░     \n"
    # logo += "                                                       ░                  ░                        "

    if clearScreen:
        print("\033[2J") # Clear screen
        print("\033[00H") # Move cursor to top left

    print("\033[36m") # Change color to blue
    print(logo)
    print("Variational Autoencoder")
    print("\033[37m") # Change color to white

    option = input(
        "-----------------\n"
        "   Input menu   \n"
        "-----------------\n"
        "1: Train\n"
        "2: Load weights\n"
        "3: Get losses\n"
        "4: Test input\n"
        "5: Plot anomaly distributions\n"
        "6: Plot ROC curve\n"
        "7: Confusion matrix\n"
        "0: Quit\n"
        ": "
        )

    return option

if __name__ == "__main__":
    # =========================================================
    #              Training with list of labels
    # =========================================================
    dataset = "black-hole"

    epochs = 100
    
    train = True

    # Load the architecture
    if dataset == "mnist":
        architecture = mnist_architecture()
    elif dataset == "cifar10":
        architecture = cifar10_architecture()
    elif dataset == "crack":
        architecture = crack_architecture()
    elif dataset == "black-hole":
        architecture = black_holes_architecture()
    else:
        print("Invalid dataset chosen, try again.")

    target_size = architecture.target_size
    latent_dim = architecture.latent_dim
    labels = architecture.labels
    beta = architecture.beta
    anomaly_labels = architecture.anomaly_labels

    BATCH_SIZE = architecture.batch_size
    learning_rate = architecture.learning_rate
    optimiser = architecture.optimiser

    checkpoint_dir = "./checkpoints/" + dataset + "/"
    image_dir = "./images/" + dataset + "/"

    for i in range(len(anomaly_labels)):
        num_tests = 3

        roc_values = []

        for k in range(num_tests):
            anomaly_classes = [anomaly_labels[i]]
            normal_classes = [x for x in labels if x not in anomaly_classes]

            # Load the model
            model = betaCVAE(
                latent_dim=latent_dim,
                beta=beta,
                image_dir=image_dir + anomaly_labels[i] + "-anomaly/",
                checkpoint_dir=checkpoint_dir + anomaly_labels[i] + "-anomaly/"
            )

            model.encoder = architecture.get_encoder()
            model.decoder = architecture.get_decoder()

            # Create or clean the directories
            if os.path.isdir(model.checkpoint_dir):
                if train:
                    os.system("rm -rf " + model.checkpoint_dir + "*")
            else:
                if not os.path.isdir(checkpoint_dir):
                    os.mkdir(checkpoint_dir)

                if not os.path.isdir(model.checkpoint_dir):
                    os.mkdir(model.checkpoint_dir)

            if os.path.isdir(model.image_dir):
                if train:
                    os.system("rm " + model.image_dir + "*")
            else:
                if not os.path.isdir(image_dir):
                    os.mkdir(image_dir)

                if not os.path.isdir(model.image_dir):
                    os.mkdir(model.image_dir)

            # Load the dataset
            if dataset == "mnist":
                (train_dataset, validation_dataset), (test_normal_dataset, test_anomaly_dataset, test_total_dataset) = mnist_dataset.load(
                    batch_size=BATCH_SIZE,
                    target_size=target_size,
                    classes=labels,
                    normal_classes=normal_classes,
                    anomaly_classes=anomaly_classes
                )
            elif dataset == "cifar10":
                (train_dataset, validation_dataset), (test_normal_dataset, test_anomaly_dataset, test_total_dataset) = cifar10_dataset.load(
                    batch_size=BATCH_SIZE,
                    target_size=target_size,
                    classes=labels,
                    normal_classes=normal_classes,
                    anomaly_classes=anomaly_classes
                )
            elif dataset == "crack":
                (train_dataset, validation_dataset), (test_normal_dataset, test_anomaly_dataset, test_total_dataset) = crack_dataset.load(
                    batch_size=BATCH_SIZE,
                    target_size=target_size,
                    crack_set=labels[i],
                    classes=["cracked", "non-cracked"],
                    normal_classes=["non-cracked"],
                    anomaly_classes=["cracked"]
                )
            elif dataset == "black-hole":
                (train_dataset, validation_dataset), (test_normal_dataset, test_anomaly_dataset, test_total_dataset) = black_hole_dataset.load(
                    batch_size=BATCH_SIZE,
                    target_size=target_size,
                    classes=labels,
                    normal_classes=["positive"],
                    anomaly_classes=["negative"]
                )
            else:
                print("Invalid dataset chosen, try again.")
                break

            # Training and benchmarking
            with tf.device("/GPU:0"):
                if train:
                    model.train(train_dataset, validation_dataset, epochs, learning_rate, optimiser, verbose=1)

                model.load_weights("./" + model.checkpoint_dir + "weights_last")

                roc = utils.plot_ROC(model=model, normal_dataset=test_normal_dataset, anomaly_dataset=test_anomaly_dataset, verbose=1)

                if k == num_tests-1:
                    utils.plot_score_distribution(model=model, normal_dataset=test_normal_dataset, anomaly_dataset=test_anomaly_dataset, num_samples=800)

                    utils.confusion_matrix(model=model, normal_dataset=test_normal_dataset, anomaly_dataset=test_anomaly_dataset)

                    utils.visualiseLatentSpace(model, test_total_dataset)

            roc_values.append(roc)

        roc_best = max(roc_values)
        roc_mean = np.mean(roc_values)
        roc_stdev = np.std(roc_values)

        print("{}-anomaly: Best = {}, Mean = {}, Standard deviation = {}".format(anomaly_labels[i], roc_best, roc_mean, roc_stdev))