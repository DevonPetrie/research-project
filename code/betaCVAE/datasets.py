import tensorflow as tf

class mnist_dataset():
    def load(batch_size, target_size, classes, normal_classes, anomaly_classes):
        train_image_generator = tf.keras.preprocessing.image.ImageDataGenerator(
            rescale=1./255,
            validation_split=0.2
        )

        test_image_generator = tf.keras.preprocessing.image.ImageDataGenerator(
            rescale=1./255,
        )

        # Train dataset
        train_dataset = train_image_generator.flow_from_directory(
            directory="../datasets/mnist/train/",
            batch_size=batch_size,
            shuffle=True,
            target_size=target_size,
            classes=normal_classes,
            subset="training"
        )

        # Validation dataset
        validation_dataset = train_image_generator.flow_from_directory(
            directory="../datasets/mnist/train/",
            batch_size=batch_size,
            shuffle=True,
            target_size=target_size,
            classes=normal_classes,
            subset="validation"
        )

        # Test (generalisation datasets)
        test_normal_dataset = test_image_generator.flow_from_directory(
            directory="../datasets/mnist/test/",
            batch_size=batch_size,
            shuffle=True,
            target_size=target_size,
            classes=normal_classes
        )

        test_anomaly_dataset = test_image_generator.flow_from_directory(
            directory="../datasets/mnist/test/",
            batch_size=batch_size,
            shuffle=True,
            target_size=target_size,
            classes=anomaly_classes
        )

        test_total_dataset = test_image_generator.flow_from_directory(
            directory="../datasets/mnist/test/",
            batch_size=batch_size,
            shuffle=True,
            target_size=target_size,
            classes=classes
        )

        return (train_dataset, validation_dataset), (test_normal_dataset, test_anomaly_dataset, test_total_dataset)

class cifar10_dataset():
    def load(batch_size, target_size, classes, normal_classes, anomaly_classes):
        train_image_generator = tf.keras.preprocessing.image.ImageDataGenerator(
            rescale=1./255,
            validation_split=0.2
        )

        test_image_generator = tf.keras.preprocessing.image.ImageDataGenerator(
            rescale=1./255,
        )

        # Train dataset
        train_dataset = train_image_generator.flow_from_directory(
            directory="../datasets/cifar-10/train/",
            batch_size=batch_size,
            shuffle=True,
            target_size=target_size,
            classes=normal_classes,
            subset="training"
        )

        # Validation dataset
        validation_dataset = train_image_generator.flow_from_directory(
            directory="../datasets/cifar-10/train/",
            batch_size=batch_size,
            shuffle=True,
            target_size=target_size,
            classes=normal_classes,
            subset="validation"
        )

        # Test (generalisation datasets)
        test_normal_dataset = test_image_generator.flow_from_directory(
            directory="../datasets/cifar-10/test/",
            batch_size=batch_size,
            shuffle=True,
            target_size=target_size,
            classes=normal_classes
        )

        test_anomaly_dataset = test_image_generator.flow_from_directory(
            directory="../datasets/cifar-10/test/",
            batch_size=batch_size,
            shuffle=True,
            target_size=target_size,
            classes=anomaly_classes
        )

        test_total_dataset = test_image_generator.flow_from_directory(
            directory="../datasets/cifar-10/test/",
            batch_size=batch_size,
            shuffle=True,
            target_size=target_size,
            classes=classes
        )

        return (train_dataset, validation_dataset), (test_normal_dataset, test_anomaly_dataset, test_total_dataset)

class crack_dataset():
    def load(batch_size, target_size, crack_set, classes, normal_classes, anomaly_classes):
        train_image_generator = tf.keras.preprocessing.image.ImageDataGenerator(
            rescale=1./255,
            validation_split=0.2
        )

        test_image_generator = tf.keras.preprocessing.image.ImageDataGenerator(
            rescale=1./255,
        )

        # Train dataset
        train_dataset = train_image_generator.flow_from_directory(
            directory="../datasets/crack/" + crack_set + "/",
            batch_size=batch_size,
            shuffle=True,
            target_size=target_size,
            classes=normal_classes,
            subset="training"
        )

        # Validation dataset
        validation_dataset = train_image_generator.flow_from_directory(
            directory="../datasets/crack/" + crack_set + "/",
            batch_size=batch_size,
            shuffle=True,
            target_size=target_size,
            classes=normal_classes,
            subset="validation"
        )

        # Test (generalisation datasets)
        test_normal_dataset = test_image_generator.flow_from_directory(
            directory="../datasets/crack/" + crack_set + "/",
            batch_size=batch_size,
            shuffle=True,
            target_size=target_size,
            classes=normal_classes
        )

        test_anomaly_dataset = test_image_generator.flow_from_directory(
            directory="../datasets/crack/" + crack_set + "/",
            batch_size=batch_size,
            shuffle=True,
            target_size=target_size,
            classes=anomaly_classes
        )

        test_total_dataset = test_image_generator.flow_from_directory(
            directory="../datasets/crack/" + crack_set + "/",
            batch_size=batch_size,
            shuffle=True,
            target_size=target_size,
            classes=classes
        )

        return (train_dataset, validation_dataset), (test_normal_dataset, test_anomaly_dataset, test_total_dataset)

class black_hole_dataset():
    def load(batch_size, target_size, classes, normal_classes, anomaly_classes):
        train_image_generator = tf.keras.preprocessing.image.ImageDataGenerator(
            rescale=1./255,
            validation_split=0.2
        )

        test_image_generator = tf.keras.preprocessing.image.ImageDataGenerator(
            rescale=1./255,
        )

        # Train dataset
        train_dataset = train_image_generator.flow_from_directory(
            directory="../datasets/black-holes/",
            batch_size=batch_size,
            shuffle=True,
            target_size=target_size,
            classes=normal_classes,
            subset="training"
        )

        # Validation dataset
        validation_dataset = train_image_generator.flow_from_directory(
            directory="../datasets/black-holes/",
            batch_size=batch_size,
            shuffle=True,
            target_size=target_size,
            classes=normal_classes,
            subset="validation"
        )

        # Test (generalisation datasets)
        test_normal_dataset = test_image_generator.flow_from_directory(
            directory="../datasets/black-holes/",
            batch_size=batch_size,
            shuffle=True,
            target_size=target_size,
            classes=normal_classes
        )

        test_anomaly_dataset = test_image_generator.flow_from_directory(
            directory="../datasets/black-holes/",
            batch_size=batch_size,
            shuffle=True,
            target_size=target_size,
            classes=anomaly_classes
        )

        test_total_dataset = test_image_generator.flow_from_directory(
            directory="../datasets/black-holes/",
            batch_size=batch_size,
            shuffle=True,
            target_size=target_size,
            classes=classes
        )

        return (train_dataset, validation_dataset), (test_normal_dataset, test_anomaly_dataset, test_total_dataset)