for i in 0 1 2 3 4 5 6 7 8 9
do
    mkdir datasets/mnist-combined/$i
    cp datasets/mnist/test/$i/* datasets/mnist-combined/$i/
    cp datasets/mnist/train/$i/* datasets/mnist-combined/$i/
done