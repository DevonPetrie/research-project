\section{Methodology}
This section describes the methodology behind developing the Adversarial Disentangled Autoencoders for anomaly detection (ADAnomaly). A discussion of the ADAnomaly network is given in Section \ref{sec:ADAnomalNetwork} and a discussion of the benchmarking and performance metrics are given in Section \ref{sec:Benchmarking}

\subsection{ADAnomaly network}
\label{sec:ADAnomalNetwork}
This section will provide a discussion of the architecture and methodology behind training the ADAnomaly network. The architecture is discussed in Section \ref{sec:ADAnomalyArchitecture}, the training method is discussed in Section \ref{sec:ADAnomalyTraining} and a description of the anomaly metrics used are described in Section \ref{sec:AnomalyMetrics}.

\subsubsection{Architecture}
\label{sec:ADAnomalyArchitecture}
ADAnomaly is a proposed network architecture that will make use of two sub-networks that consist of disentangled autoencoders, these sub-networks will form the generator and discriminator of the GAN.

Each subnetwork consists of only convolutional layers, reducing the number of parameters to learn and resulting in a robust model. As proposed by Springenberg in 2014 \cite{springenberg2014striving}, both sub-networks will only make use of convolutional layers with strides instead of pooling layers. This has been shown to aid in the image reconstruction quality and it allows the network to learn its own spatial downsampling.

\subsubsection{Training}
\label{sec:ADAnomalyTraining}
In order to maintain the training roles of the respective GAN sub-networks, the adversarial training objective will be defined as the pixel-wise error between \(G(x)\) (the reconstructed image of data x through G) and \(D(G(x))\) (the generated image through D) as proposed in ADAE \cite{ADAE}:

~\\
\[|| G(\textbf{x}) - D(G(\textbf{x})) ||_1\]

~\\
Where \(||\ \ ||_1\) represents the 1-norm, this is calculated as the sum of the absolute values of a vector.

The discriminator sub-network (\(D\)) will be trained to maximise this error. This will lead to \(D\) failing to reconstruct the inputs if they belong to \(p_g\) (the generated distribution). The generator is then trained to minimise this error, training it to produce images in the real data distribution in order to attempt to ``trick" the discriminator.

The discriminator's training objective is to successfully reconstruct real inputs and fail to reconstruct generated inputs, described below:

~\\
\[\mathcal{L}_D = || \textbf{x} - D(\textbf{x}) ||_1 - || G(\textbf{x}) - D(G(\textbf{x})) ||_1\]

~\\
The generator's training objective is to not only reconstruct the real inputs, but also to attempt to match \(p_z\) to the real data distribution, described below:

~\\
\[\mathcal{L}_G = || \textbf{x} - G(\textbf{x}) ||_1 + || G(\textbf{x}) - D(G(\textbf{x})) ||_1\]

~\\
Since both networks have similar capabilities, as proposed by Sainburg \textit{et al.} \cite{sainburg2018generative}, balanced learning rates for both networks will be used in order to improve training stability.

\subsubsection{Anomaly detection}
\label{sec:AnomalyMetrics}

The anomaly score used for this method will be the reconstruction loss of the discriminator. This is done since when an anomalous image is passed through the generator, it will fail to reconstruct the image accurately. The reconstruction error will then be amplified by the discriminator.

The process of determining the anomaly score is as follows: An image \textbf{x} is initially passed through \(G\) and then subsequently through \(D\) resulting in the processed image. The pixel-wise difference between the original image and the processed image is then calculated:

~\\
\[\mathcal{A}(x) = || \textbf{x} - D(G(\textbf{x})) ||_2\]

~\\
Where \(||\ \ ||_2\) represents the euclidean norm and is calculated as the squared root of the sum of the squared vector values.

In order to determine if an image is an anomaly or not, an anomaly threshold is defined as \(\phi\). The anomaly score is compared to this threshold in order to make the distinction between an anomalous image and a normal image.

~\\
Therefore, an image \(\textbf{x}\) is considered to contain anomalous data if \(\mathcal{A}(\textbf{x}) > \phi\).

\subsection{Benchmarking and performance metrics}
\label{sec:Benchmarking}
Once all the images in the test set have been given an anomaly score and therefore a label, a receiver operating characteristic (ROC) curve can be plotted to illustrate the classifiers ability as the threshold of the classifier is changed.

The ROC curve is plotted with the true positive rate (TPR) on the y-axis against the false positive rate (FPR) on the x-axis where:

~\\
\[TPR = \frac{TP}{TP + FN}\]

~\\
and,

\[FPR = \frac{FP}{TN + FP}\]

~\\
Where, \textit{TP} represents the number of true positives, a true positive is an outcome for which a model correctly predicts a positive class. \textit{TN} represents the number of true negatives, a true negative is an outcome for which a model correctly predicts a negative class. \textit{FP} represents the number of false positive, a false positive is an outcome for which a model incorrectly predicts a positive class. \textit{FN} represents the number of false negatives, a false negative is an outcome for which a model incorrectly predicts a negative class.

The area under ROC (AUROC) represents how well the model is capable of distinguishing between classes. Therefore the measure of separability can be measured for a set of results \(x\) as:

\[AUROC = \int^1_0f(x)\ dx\]

~\\
Where \(f\) represents the ROC curve.

As the classifiers probability of predicting a TP value rather than a FP value increases, the AUROC value will tend towards 1. An AUROC value of 0.5 results from a completely random classifier and a value of 1 results from a perfect classifier. A value below 0.5 shows that the classifiers is able to classify the data, however, the prediction values are inversed and means that the classifier has the class labels reversed.