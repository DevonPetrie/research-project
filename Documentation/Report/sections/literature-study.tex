\section{Literature Study}
This section presents a literature study on the existing materials that have preceeded this proposed research. Background information on the models and previous works are also described in this section.

Section \ref{sec:BlackHoles} provides a discussion on black holes. Section \ref{sec:CNNs} covers the basic principles of convolutional neural networks. Section \ref{sec:DeepGenerativeModels} covers the different deep generative models used in this paper along with their respective principles.

\subsection{Black holes}
\label{sec:BlackHoles}
Black holes are regions in spacetime where the force of gravity is so strong that nothing can escape it. This is caused when a tremendous amount of matter has collapsed into an infinitely small space, known as a gravitational singularity.

This singularity forms the center of a black hole. At this singularity density and gravity become infinite, causing space-time to curve infinitely.

The matter that spins around the black hole has both gravitational and frictional forces acting on it as the matter spirals inwards towards the gravitational singularity. These gravitational and frictional forces compress and raise the temperate of the matter, creating what is known as an accretion disk. This is the area of the black hole where we can observe the black holes spin and forms the only area about a black hole for us to observe in the visible spectrum.

\subsection{Convolutional neural networks}
\label{sec:CNNs}
Convolutional neural networks (CNNs) are a category of neural networks that take inspiration from biology. They are inspired by the visual cortex which contains regions of cells that are sensitive to certain areas in the visual field. In 1962 Hubel and Wiesel showed that individual neurons in the brain would fire only in the presence of edges in certain orientations \cite{HubelWiesel}.

At the heart of most image recognition or classification problem today lies a CNN, this is due to their effectiveness in processing images and the fact that images fed into the network do not require flattening or much prepocessing.

CNNs were first proposed in 1998 by LeCunn \textit{ et al.} \cite{LeCunn} to classify handwritted characters. CNNs were able to outperform various other classification methods defined at the time when classifying characters from the MNIST dataset. 

Even with this discovery, CNNs were only popularised in 2012 by Krizhevsky \textit{ et al.} \cite{NIPS2012_4824} when they achieved better error rates on the ImageNet dataset than the previous state-of-the-art methods. The CNN achieved a top-5 error rate of 15.3\%. This was a substantial improvement of 10.9\% from the previously best published results with an error rate of 26.2\%.

\subsubsection{Architecture}
At the heart of a CNN lies \textit{convolutional layers}, these layers are based on a convolutional matrix (or \textit{kernel}) that slides (or convolves) over an image. This kernel is used to treat an image with various effects, such as blurring, edge detection, sharpening and so on. These effects vary based on the kernel's values.

The \textit{kernel} is usually a two-dimensional matrix that is of a smaller size than the input image. As the kernel is convolving over the input image, it computes an element wise multiplication of the values in the matrix and the input images pixel values. The sum of these multiplications is then passed through the desired activation function, resulting in a single pixel value for the output image. A visualisation of this process is given in Figure \ref{fig:kernelVisualisation}. Each position of the kernel results in an output pixel and each of these outputs is collected in what is known as the \textit{feature map}.

\begin{figure}[ht!]
   \centering
   
   \includegraphics[scale=0.6]{./assets/images/kernelVisualisation.png}

   \caption{Visualisation of the first step for a kernel passing over an image. Sourced from ``Neural Networks and Deep Learning" by Michael Nielsen \cite{MichaelAdvances}.}

   \label{fig:kernelVisualisation}
\end{figure}

After each step the kernel is moved across the image, the rate at which the kernel moves is known as the \textit{stride}. Since the kernel is confined within the input image when there is no padding added, the exterior pixels will not be processed which will result in the output image to be smaller than the input image. A solution to this problem is to add more pixels surrounding the image. There are 3 common techniques for doing so, such as padding zeroes, wrapping around the image, or extending (duplicating) the bordering pixels.

A pooling layer can be used in order to progressively reduce the spatial size of the representation in the network and to reduce the networks number of parameters. The most common type of pooling layer used is \textit{max pooling} which calculates the maximum value in each patch of the feature map and then uses that value in the creation of a smaller feature map.

The power of CNNs comes from the convolutional layers ability to extract features from an image by using low dimensional kernels, with the fully connected layers then using these features to further build a model of the problem space.

\subsection{Deep generative models}
\label{sec:DeepGenerativeModels}
Deep learning was first introduced by Ivakhnenko and Lapa in 1967 \cite{DeepLearning-Alexey} but was only later popularised in 2015 by LeCunn \textit{et al.} \cite{DeepLearning-LeCun}. These models are a class of unsupervised learning where the goal of the model is to learn the data distribution well enough in order to then be able to generate new samples from the same distribution.

Where discriminative models learn to map the boundary between classes, generative models instead learn to model the distribution of those classes. This allows generative models to then sample from their learned distribution in order to generate seemingly new and unseen data.

The three classes of deep generative models discussed in this paper are variational autoencoders, disentangled variational autoencoders and generative adversarial networks. These models will be discussed in sections \ref{sec:VAEs}, \ref{sec:DVAEs} and \ref{sec:GANs}, respectively.

\subsubsection{Variational autoencoders}
\label{sec:VAEs}
Variational autoencoders (VAEs) were first introduced in 2013 by Diederik Kingma and Max Welling \cite{kingma2013autoencoding} and are a recently emerging family for generative models. A VAE is an unsupervised learning algorithm as they do not require any labelled data to learn, instead they learn to recreate the original input as their output.

A VAE consists mainly of two networks, the encoder and the decoder. Both of these networks are neural networks and may therefore consist of convolutional layers.

The encoder portion of the network takes the input and maps it to a latent representation \textbf{z} consisting of a set of mean (\(\mu\)) and standard deviation (\(\sigma\)) parameters, the encoder can therefore be denoted by \(q_\phi(\textbf{z} | \textbf{x})\). This latent representation is stochastic and represents a gaussian distribution.

The decoder of a VAE takes a sample from the latent representation \textbf{z} and outputs the parameters to the probability distribution, denoted by \(p_\theta(\textbf{x} | \textbf{z})\).

The error for a dataset \textbf{x} of samples and \textbf{z} of ground truth factors can be calculated by maximising the evidence lower bound on the marginal log-likelihood.

\[\mathcal{L}(\theta, \phi; \textbf{x}, \textbf{z}) = \mathbb{E}_{q_\phi(\textbf{z}|\textbf{x})} [log\ p_\theta(\textbf{x}|\textbf{z})] - D_{KL}(q_\phi(\textbf{z}|\textbf{x})\ ||\ p(\textbf{z}))\]

~\\
Where \(\phi,\ \theta\) parametrise the distributions of the encoder and decoder respectively, \(\mathbb{E}_{q_\phi(\textbf{z}|\textbf{x})} [\ ]\) represents the reconstruction loss and \(D_{KL}(\ ||\ )\) represents the non-negative KL divergence. This KL divergence measures how different a probability distribution is from a second, reference probability distribution. This is used in an attempt to force the latent distribution to have a mean and standard deviation relatively close to 0 and 1.

\subsubsection*{Reparameterization trick}
The sampling from a distribution in order to feed values into the decoder portion of the network brings up an issue with VAEs. The sampling node restricts backbropagation through the network as gradients cannot be pushed through a sampling node. 

In order to train the network and run the gradients through the entire network the \textit{reparameterization trick} is required, which is illustrated in Figure \ref{fig:reparameterizationTrick}.

\begin{figure}[ht!]
   \centering
   
   \includegraphics[scale=0.25]{./assets/images/reparamTrick.png}

   \caption{Visualisation of the reparameterization trick. Image sourced from the MIT lecture on Deep Generative Modeling \cite{DeepGenerativeModelingLecture}.}

   \label{fig:reparameterizationTrick}
\end{figure}

The reparameterization trick splits the stochastic sampling node that blocks the gradients into two parts, one part where you can perform backbropagation through and another part which is still stochastic but which doesn't require training due to the fact that it is fixed. This new sampling vector can then be viewed as:

\[\textbf{z} = \mu + \sigma\odot\epsilon\]

~\\
Where \(\mu\) and \(\sigma\) are both parameters that the network is learning, \(\epsilon\) is the stochastic node. \(\epsilon \sim N(0,1)\) and \(\odot\) is defined as a component wise multiplication.

\subsubsection{Disentangled variational autoencoders}
\label{sec:DVAEs}
As proposed by Higgins \textit{et al.} \cite{BetaVAE} in 2017, a separate hyperparameter \(\beta\) can be added to the KL divergence of the VAE loss function in order to create a disentangled autoencoder.

The hyperparameter \(\beta\) restricts the network and creates a disentangled representation of the data distribution, making sure that the different neurons in the latent representation are uncorrelated and that each neuron attempts to learn something different about the input data.

The way that this extra hyperparameter removes the correlation between neurons is by acting as a ``mixing coefficient" for the VAE lower bound formula. This \(\beta\) term balances the magnitudes of the gradients from the reconstruction loss and the KL divergence terms, resulting in disentangled representations of the dataset's generalisation factors.

The resulting loss function will therefore become:

\[\mathcal{L}(\theta, \phi; \textbf{x}, \textbf{z}) = \mathbb{E}_{q_\phi(\textbf{z}|\textbf{x})} [log\ p_\theta(\textbf{x}|\textbf{z})] - \beta D_{KL}(q_\phi(\textbf{z}|\textbf{x})\ ||\ p(\textbf{z}))\]

\subsubsection{Generative adversarial networks}
\label{sec:GANs}
Goodfellow \textit{et al.} \cite{Goodfellow-GAN} first proposed the methodology of generative adversarial networks in 2014 by co-training a pair of networks referred to as the generator \textit{G} and the discriminator \textit{D}.

To learn the generator's distribution \(p_g\) over the data \(x\), a prior input noise variable \(p_z(z)\) is defined. \(D(x)\) returns a probability that represents the likelihood that \(x\) came from the data rather than from \(p_g\). The training for \textit{D} is done to maximise the probability that it assigns the correct label to both the training samples and the samples from \textit{G}. \textit{G} is simultaneously trained in order to minimise \(log(1 - D(G(z)))\):

\[\min_{G} \max_{D} V(D, G) = \mathbb{E}_{\textbf{x} \sim p_{data}(\textbf{x})}[logD(\textbf{x})] - \mathbb{E}_{\textbf{z} \sim p_\textbf{z}(\textbf{z})} [log(1 - D(G(\textbf{z})))]\]

~\\
A visualisation of the process described is depicted in Figure \ref{fig:GANVisualisation} and the summary of training a GAN is described in Algorithm \ref{alg:GANTraining}, sourced from Goodfellow et al.

\begin{figure}[ht!]
   \centering
   
   \includegraphics[scale=0.6]{./assets/images/gan-visual.png}

   \caption{Visualisation of a GAN. Source: \textit{Keep Calm and train a GAN. Pitfalls and Tips on training Generative Adversarial Networks} by Utkarsh Desai \cite{GANTraining}}

   \label{fig:GANVisualisation}
\end{figure}

\begin{algorithm}[H]
   \label{alg:GANTraining}
   \SetAlgoLined
    \For{number of training iterations}
    {
      \For{\textit{k} steps}
      {
         \(\bullet\) Sample minibatch of \textit{m} noise samples \(\{\textbf{z}^{(1)}, ..., \textbf{z}^{(m)}\}\) from noise prior \(p_g(\textbf{z})\).

         \(\bullet\) Sample minibatch of \textit{m} examples \(\{\textbf{x}^{(1)}, ..., \textbf{x}^{(m)}\}\) from data generating distribution \(p_{data}(\textbf{x})\).

         \(\bullet\) Update the discriminator by ascending its stochastic gradient:

         \[\nabla_{\theta_d} \frac{1}{m} \sum^m_{i=1}[log\ D(\textbf{x}^{(i)}) + log(1 - D(G(\textbf{z}^{(i)})))]\]
      }

      \(\bullet\) Sample minibatch of \textit{m} noise samples \(\{\textbf{z}^{(1)}, ..., \textbf{z}^{(m)}\}\) from noise prior \(p_g(\textbf{z})\).

      \(\bullet\) Update the generator by descending its stochastic gradient:

      \[\nabla_{\theta_g} \frac{1}{m} \sum^m_{i=1} log(1 - D(G(\textbf{z}^{(i)})))\]
    }
    
    The gradient-based updates can use any standard gradient-based learning rule. We used momentum in our experiments.
 
    \caption{Minibatch stochastic gradient descent training of generative adversarial nets. The number of steps to apply to the discriminator, \textit{k}, is a hyperparameter. We used \(k = 1\), the least expensive option, in our experiments.}
\end{algorithm}