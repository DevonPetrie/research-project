\section{Methodology}
This section describes the methodology behind developing the Generative Companions for Anomaly Detection (GCAD). A discussion of the GCAD network is given in Section \ref{sec:GCADNetwork}, a discussion on the constructing of the black hole dataset is given in Section \ref{sec:BuildingTheDataset}, and a discussion of the benchmarking and performance metrics are given in Section \ref{sec:Benchmarking}

\subsection{GCAD network}
\label{sec:GCADNetwork}
This subsection provides a discussion of the architecture and methodology behind training the GCAD network. The architecture is discussed in Section \ref{sec:GCADArchitecture}, the training method is discussed in Section \ref{sec:GCADTraining} and a description of the anomaly metrics used are described in Section \ref{sec:AnomalyMetrics}.

\subsubsection{Architecture}
\begin{figure}[ht!]
    \centering
    
    \includegraphics[scale=0.13]{./assets/images/pipeline.png}
 
    \caption{Visualisation of the GCAD pipeline.}
 
    \label{fig:GCADPipeline}
 \end{figure}

\label{sec:GCADArchitecture}
GCAD is a proposed network architecture that makes use of two sub-networks that consist of disentangled autoencoders. These sub-networks form the generator and discriminator of the network as shown in Figure \ref{fig:GCADPipeline}.

Each subnetwork consists of only convolutional layers, reducing the number of parameters to learn and resulting in a robust model. As proposed by Springenberg in 2014 \cite{springenberg2014striving}, both sub-networks only make use of convolutional layers with strides instead of pooling layers. This has been shown to aid in the image reconstruction quality and it allows the network to learn its own spatial downsampling.

\subsubsection{Training}
\label{sec:GCADTraining}
Both sub-networks in the model are trained using the VAE loss function described in Section \ref{sec:VAEs}. In addition to the VAE training function, the sub-networks are also trained with an additional term called the \textit{companion loss}.

As opposed to in a GAN where the generator attempts to ``trick" the discriminator, in GCAD the sub-networks aim to further reinforce each others training on the original data by also having each companion minimise the reconstruction loss of an image that is initially passed through the other companion. Thus allowing them to learn a more coupled representation of the data distribution while also causing them to receive more training data as each sub-network will slightly augment an image passed through it.

The companion loss for the generator is then given as the VAE loss plus the reconstruction loss of D(x) and G(D(X)), resulting in the generators loss function being:

~\\
\[
    \mathcal{L}_G(\theta, \phi; \textbf{x}, \textbf{z}) = \mathbb{E}_{q_\phi(\textbf{z}|\textbf{x})} [log\ p_\theta(\textbf{x}|\textbf{z})]\ -\ \beta D_{KL}(q_\phi(\textbf{z}|\textbf{x})\ ||\ p(\textbf{z}))\ +\ || D(\textbf{x}) - G(D(\textbf{x})) ||_2
\]

~\\
Where \(||\ \ ||_2\) represents the Euclidean norm and is calculated as the squared root of the sum of the squared vector values.

The companion loss for the discriminator is then given as the VAE loss plus the reconstruction loss of G(x) and D(G(X)), resulting in the discriminators loss function being:

~\\
\[\mathcal{L}_D(\theta, \phi; \textbf{x}, \textbf{z}) = \mathbb{E}_{q_\phi(\textbf{z}|\textbf{x})} [log\ p_\theta(\textbf{x}|\textbf{z})]\ -\ \beta D_{KL}(q_\phi(\textbf{z}|\textbf{x})\ ||\ p(\textbf{z}))\ +\ || G(\textbf{x}) - D(G(\textbf{x})) ||_2\]

\subsubsection{Anomaly detection}
\label{sec:AnomalyMetrics}

The anomaly score used for this method will be the reconstruction loss of an image that is passed through the generator and then subsequently through the discriminator. This is done since when an anomalous image is passed through the generator, it will fail to reconstruct the image accurately. The reconstruction error will then be amplified by the discriminator.

The process of determining the anomaly score is as follows: An image \textbf{x} is initially passed through \(G\) and then subsequently through \(D\) resulting in the processed image. The reconstruction loss between the original image and the processed image is then calculated:

~\\
\[\mathcal{A}(x) = || \textbf{x} - D(G(\textbf{x})) ||_2\]

~\\
In order to determine if an image is an anomaly or not, an anomaly threshold is defined as \(\phi\). The anomaly threshold is selected dynamically for each problem dataset as the threshold that resulted in a point on the ROC curve that is closest to the point (0, 1), i.e. the threshold with the highest true positive rate and the lowest false positive rate. The anomaly score is compared to this threshold in order to make the distinction between an anomalous image and a normal image.

~\\
Therefore, an image \(\textbf{x}\) is considered to contain anomalous data if \(\mathcal{A}(\textbf{x}) > \phi\).

\subsection{Building the network}
\label{sec:BuildingTheNetwork}
A bottom up approach to the problem is taken, beginning by constructing a standard variational autoencoder (VAE), then proceeding to add and select the \(\beta\) hyperparameter in order to construct the \(\beta\)-VAE, until finally a combination of two of these networks is taken to form the GCAD network described above.

The VAE is constructed by employing a genetic algorithm to perform a multi-point search on the search space of the problem. This is done as to evolve a list of hyperparameters to use for the network, these parameters will be used in subsequent models that build ontop of the VAE. These hyperparameters include: The number of layers, the number of filters per layer, the optimiser used (either adam or rmsprop), the learning rate, the kernel size, and the latent dimension size. The genetic algorithm follows a generational control model and uses the reproduction, crossover and mutation genetic operators to perform the search. Due to the complexity of training multiple models, a population size of 9 is used and ran for only 3 generations. The GA is therefore used to simply train multiple models with a random distribution of selected parameters to select the ones that performed the best while also using the genetic operators to perform some informed moves in the search space between generations.

Once the VAE is constructed and benchmarked, the \(\beta\)-VAE is constructed by adding the \(\beta\) hyperparameter to the KL divergence term of the VAE loss function. This hyperparameter is then tuned by training the model again with a range of \(\beta\) values and the value which resulted in the highest AUROC value is selected to move forward with.

The GCAD network is then built by employing two \(\beta\)-VAEs as generator and discriminator in the network. An extra loss metric is added to each of the sub-networks training objectives in order to perform the desired companion training.

The GCAD network is then slightly tuned in the end in order to optimise it for the black hole dataset. This entire approach is done in order to illustrate the versatility of the model developed and to show the constructive approach used to build the model. This also allows a comparison of each of the models used in the process to develop the final GCAD network to be drawn.

\subsection{Building the dataset}
\label{sec:BuildingTheDataset}
The astronomical data used in this paper comes from the \textit{ASTRO-CIRG} research group and consists of images captured during simulations of black holes. The simulations were made with taking 8 parameters into account, most notibly for this paper is the black holes spin.

There are 100 000 images in the supplied dataset, and it was chosen that the data will be split on the spin parameter. This resulted in two classes for the data: Positive spins (80 000 images) and negative spins (20 000 images). The negative spin black holes were taken as the anomaly class as they are suspected to be rarer than their positive spin siblings.

Examples of 4 positive and 4 negative spin black hole images are given in Figure \ref{fig:positiveBlackHoleSpins} and Figure \ref{fig:negativeBlackHoleSpins}, respectively.

\begin{figure}[ht!]
    \centering
    
    \includegraphics[scale=0.6]{./assets/images/bh_positive_0.png}
    \hspace{0.5cm}
    \includegraphics[scale=0.6]{./assets/images/bh_positive_1.png}
    \hspace{0.5cm}
    \includegraphics[scale=0.6]{./assets/images/bh_positive_2.png}
    \hspace{0.5cm}
    \includegraphics[scale=0.6]{./assets/images/bh_positive_3.png}
 
    \caption{Images taken from the black hole simulations with a positive spin.}
 
    \label{fig:positiveBlackHoleSpins}
\end{figure}

\begin{figure}[ht!]
    \centering
    
    \includegraphics[scale=0.6]{./assets/images/bh_negative_0.png}
    \hspace{0.5cm}
    \includegraphics[scale=0.6]{./assets/images/bh_negative_1.png}
    \hspace{0.5cm}
    \includegraphics[scale=0.6]{./assets/images/bh_negative_2.png}
    \hspace{0.5cm}
    \includegraphics[scale=0.6]{./assets/images/bh_negative_3.png}

    \caption{Images taken from the black hole simulations with a negative spin.}

    \label{fig:negativeBlackHoleSpins}
\end{figure}

\subsection{Benchmarking and performance metrics}
\label{sec:Benchmarking}

\subsubsection{ROC curve}
Once all the images in the test set have been given an anomaly score, a receiver operating characteristic (ROC) curve can be plotted to illustrate the classifiers ability as the threshold (\(\phi\)) of the classifier is changed. This threshold is used to give each image in the test set a label as described in section \ref{sec:AnomalyMetrics}, and as this threshold varies, the labels given will change. The true positive rate (TPR) and false positive rate (FPR) is determined for each of these thresholds by comparing the given labels to the target labels for each image.

The ROC curve is plotted with the true positive rate (TPR) on the y-axis against the false positive rate (FPR) on the x-axis where:

~\\
\[TPR = \frac{TP}{TP + FN}\]

~\\
and,

\[FPR = \frac{FP}{TN + FP}\]

~\\
Where, \textit{TP} represents the number of true positives, a true positive is an outcome for which a model correctly predicts a positive class. \textit{TN} represents the number of true negatives, a true negative is an outcome for which a model correctly predicts a negative class. \textit{FP} represents the number of false positive, a false positive is an outcome for which a model incorrectly predicts a positive class. \textit{FN} represents the number of false negatives, a false negative is an outcome for which a model incorrectly predicts a negative class.

\subsubsection{AUROC}
The area under ROC (AUROC) represents how well the model is capable of distinguishing between classes. Therefore the measure of separability can be measured for a set of results \(x\) as:

\[AUROC = \int^1_0f(x)\ dx\]

~\\
Where \(f\) represents the ROC curve.

As the classifiers probability of predicting a TP value rather than a FP value increases, the AUROC value will tend towards 1. An AUROC value of 0.5 results from a completely random classifier and a value of 1 results from a perfect classifier. A value below 0.5 shows that the classifiers is able to classify the data, however, the prediction values are inversed and means that the classifier has the class labels reversed.